Require Import Coq.Arith.Wf_nat.
Require Import Coq.Bool.Bool.
Require Import Coq.Arith.Compare_dec.
Require Import Recdef.
Require Import Coq.Setoids.Setoid.
Require Import Coq.QArith.QArith.
Require Import Interpret.
Require Import IntervalTreeMap.

Open Scope nat_scope.

Module ITC_LE.
Section Defs.

  (** Original definition of LE, as in the paper. *)

  Inductive Le : event -> event -> Prop :=
  | le_leaf:
    forall n1 n2,
    n1 <= n2 ->
    Le (Leaf n1) (Leaf n2)
  | le_leaf_lhs:
    forall n1 n2 r2 l2,
    n1 <= n2 ->
    Le (Leaf n1) (Node n2 l2 r2)
  | le_leaf_rhs:
    forall n1 n2 r1 l1,
    n1 <= n2 ->
    Le (lift l1 n1) (Leaf n2) ->
    Le (lift r1 n1) (Leaf n2) ->
    Le (Node n1 l1 r1) (Leaf n2)
  | le_node:
    forall n1 n2 l1 l2 r1 r2,
    n1 <= n2 ->
    Le (lift l1 n1) (lift l2 n2) ->
    Le (lift r1 n1) (lift r2 n2) ->
    Le (Node n1 l1 r1) (Node n2 l2 r2).

End Defs.
End ITC_LE.
(*
Module ITC_LT_EQ.

  (**
    Definition of happens-before (LT) and EQ as similar as possible to the
    paper's original definition. *)

Section Defs.
  Import ITC_LE.

  Inductive Lt : event -> event -> Prop :=
  | lt_leaf_lhs:
    forall n1 e2,
    n1 < value e2 ->
    Lt (Leaf n1) e2
  | lt_leaf_lhs_eq:
    forall n r l,
    ~ Le (Node n l r) (Leaf n) ->
    Lt (Leaf n) (Node n l r)
  | lt_leaf_rhs:
    forall n1 n2 r1 l1,
    n1 < n2 ->
    Le (lift l1 n1) (Leaf n2) ->
    Le (lift r1 n1) (Leaf n2) ->
    Lt (Node n1 l1 r1) (Leaf n2)
  | lt_node:
    forall n1 n2 l1 l2 r1 r2,
    n1 < n2 ->
    Le (lift l1 n1) (lift l2 n2) ->
    Le (lift r1 n1) (lift r2 n2) ->
    Lt (Node n1 l1 r1) (Node n2 l2 r2)
  | lt_node_l:
    forall n l1 l2 r1 r2,
    Lt (lift l1 n) (lift l2 n) ->
    Le (lift r1 n) (lift r2 n) ->
    Lt (Node n l1 r1) (Node n l2 r2)
  | lt_node_r:
    forall n l1 l2 r1 r2,
    Le (lift l1 n) (lift l2 n) ->
    Lt (lift r1 n) (lift r2 n) ->
    Lt (Node n l1 r1) (Node n l2 r2).

  Lemma lt_to_le:
    forall e1 e2,
    Lt e1 e2 ->
    Le e1 e2.
  Proof.
    intros.
    induction H.
    - apply le_leaf; auto with *.
    - apply le_leaf_lhs; auto with *.
    - apply le_leaf_lhs; auto.
    - apply le_leaf_rhs; auto with *.
    - apply le_node; auto with *.
  Qed.

  Require Import Coq.omega.Omega.

  Lemma le_to_not_le:
    forall e1 e2,
    Lt e1 e2 ->
    ~ Le e2 e1.
  Proof.
    induction e1; intros;
    unfold not; intros N.
      inversion N; subst; clear N; try omega.
    - inversion H; subst; clear H.
      omega.
    - inversion H; subst; clear H.
      + omega.
      + contradiction H5.
        apply le_leaf_rhs; auto.
    - inversion H; subst; clear H.
      + inversion N; subst.
        omega.
      + inversion N; subst; omega.
  Qed.

  Let le_to_lt_leaf:
    forall n1 e2,
    Le (Leaf n1) e2 ->
    ~ Le e2 (Leaf n1) ->
    Lt (Leaf n1) e2.
  Proof.
    intros.
    inversion H; subst; clear H.
    + apply lt_leaf.
      apply le_lt_or_eq in H2.
      destruct H2; auto.
      rewrite H in *.
      contradiction H0.
      auto using le_leaf.
    + apply le_lt_or_eq in H2.
      destruct H2 as [?|R].
      * auto using lt_leaf_lhs.
      * rewrite R in *.
        auto using lt_leaf_lhs_eq.
  Qed.

  Let le_not_leaf:
    forall n e,
    ~ Le (Leaf n) e ->
    n > value e.
  Proof.
    intros.
    destruct (le_gt_dec n (value e)). {
      contradiction H.
      destruct e; simpl in *; auto using le_leaf, le_leaf_lhs.
    }
    assumption.
  Qed.

  Let le_to_lt_leaf_rhs:
    forall e1 n2,
    Le e1 (Leaf n2) ->
    ~ Le (Leaf n2) e1 ->
    Lt e1 (Leaf n2).
  Proof.
    destruct e1; simpl in *; intros. {
      auto.
    }
    apply le_not_leaf in H0; simpl in *.
    inversion H;subst; clear H.
    apply lt_leaf_rhs; auto.
  Qed.

  Let le_to_lt_lift:
    forall e1 e2 d1 d2,
    Le (lift e1 d1) (lift e2 d2) ->
    ~ Le (lift e2 d2) (lift e1 d1) ->
    Lt (lift e1 d1) (lift e2 d2).
  Proof.
    induction e1; intros. {
      simpl in *; auto.
    }
    destruct e2; simpl in *. {
      auto.
    }
    apply lt_node.
  Qed.

  Inductive Eq : event -> event -> Prop :=
  | eq_leaf:
    forall n1 n2,
    n1 = n2 ->
    Eq (Leaf n1) (Leaf n2)
  | eq_leaf_lhs:
    forall n1 n2 r2 l2,
    n1 = n2 ->
    Eq (Leaf n1) (Node n2 l2 r2)
  | eq_leaf_rhs:
    forall n1 n2 r1 l1,
    n1 = n2 ->
    Eq (lift l1 n1) (Leaf n2) ->
    Eq (lift r1 n1) (Leaf n2) ->
    Eq (Node n1 l1 r1) (Leaf n2)
  | eq_node:
    forall n1 n2 l1 l2 r1 r2,
    n1 = n2 ->
    Eq (lift l1 n1) (lift l2 n2) ->
    Eq (lift r1 n1) (lift r2 n2) ->
    Eq (Node n1 l1 r1) (Node n2 l2 r2).

End Defs.
End ITC_LT_EQ.
*)
Module ITC_LEQ.
Section Defs.

  (**
    Alternative definition of LEQ that uses two-extra parameters instead of
    rewriting the objects, as in the original definition.
    *)

  Inductive Leq : nat ->  event -> nat -> event -> Prop :=
  | leq_leaf:
    forall n1 n2 a b,
    n1 + a <= n2 + b ->
    Leq a (Leaf n1) b (Leaf n2)
  | leq_leaf_lhs:
    forall n1 n2 r2 l2 a b,
    n1 + a <= n2 + b ->
    Leq a (Leaf n1) b (Node n2 l2 r2)
  | leq_leaf_rhs:
    forall n1 n2 r1 l1 a b,
    n1 + a <= n2 + b ->
    Leq (n1 + a) l1 b (Leaf n2) ->
    Leq (n1 + a) r1 b (Leaf n2) ->
    Leq a (Node n1 l1 r1) b (Leaf n2)
  | leq_node:
    forall n1 n2 l1 l2 r1 r2 a b,
    n1 + a <= n2 + b ->
    Leq (n1 + a) l1 (n2 + b) l2 ->
    Leq (n1 + a) r1 (n2 + b) r2 ->
    Leq a (Node n1 l1 r1) b (Node n2 l2 r2).

  Lemma lift_leaf_rw:
    forall n1 n2,
    Leaf (n1 + n2) = lift (Leaf n1) n2.
  Proof.
    intros.
    simpl; auto.
  Qed.

  Import ITC_LE.

  Let leq_to_le:
    forall e1 e2 n1 n2,
    Le (lift e1 n1) (lift e2 n2) ->
    Leq n1 e1 n2 e2.
  Proof.
    induction e1; intros; simpl in *. {
      destruct e2; inversion H; subst; clear H.
      - auto using leq_leaf.
      - auto using leq_leaf_lhs.
    }
    destruct e2; inversion H; subst; clear H; simpl in *.
    - rewrite lift_leaf_rw in *.
      apply leq_leaf_rhs; auto.
    - eauto using leq_node.
  Qed.

  Let le_to_leq:
    forall e1 e2 n1 n2,
    Leq n1 e1 n2 e2 ->
    Le (lift e1 n1) (lift e2 n2).
  Proof.
    induction e1; intros; simpl in *. {
      destruct e2;
      inversion H;
      subst;
      clear H;
      simpl; auto using le_leaf, le_leaf_lhs.
    }
    destruct e2; inversion H; subst; clear H; simpl in *.
    - apply le_leaf_rhs; try rewrite lift_leaf_rw in *; auto.
    - eauto using le_node.
  Qed.

  Let lift_0_rw:
    forall e,
    lift e 0 = e.
  Proof.
    intros.
    destruct e; simpl; auto.
    rewrite <- plus_n_O.
    trivial.
  Qed.

  Lemma leq_spec:
    forall e1 e2,
    Le e1 e2 <-> Leq 0 e1 0 e2.
  Proof.
    split; intros. {
    assert (r1: e1 = lift e1 0) by auto using lift_0_rw.
    assert (r2: e2 = lift e2 0) by auto using lift_0_rw.
    rewrite r1 in H.
    rewrite r2 in H.
    apply leq_to_le in H.
    assumption.
    }
    assert (r1: e1 = lift e1 0) by auto using lift_0_rw.
    assert (r2: e2 = lift e2 0) by auto using lift_0_rw.
    rewrite r1.
    rewrite r2.
    auto using le_to_leq.
  Qed.

End Defs.
End ITC_LEQ.

Module LE_IMPL.

  (** Recursive implementation of the algorithm in the paper, proved correct. *)

  Section Defs.
  Import ITC_LE.

  Function leb e1 e2  {measure size e1 } : bool :=
  match e1, e2 with
  | Leaf n1, Leaf n2 => if le_dec n1 n2 then true else false
  | Leaf n1, Node n2 _ _ => if le_dec n1 n2 then true else false
  | Node n1 l1 r1, Leaf n2 =>
    if le_dec n1 n2 then
      leb (lift l1 n1) (Leaf n2) && leb (lift r1 n1) (Leaf n2)
    else false
  | Node n1 l1 r1, Node n2 l2 r2 =>
    if le_dec n1 n2 then
      leb (lift l1 n1) (lift l2 n2) && leb (lift r1 n1) (lift r2 n2)
    else false
  end.
  Proof.
  auto using size_lift_r.
  auto using size_lift_l.
  auto using size_lift_r.
  auto using size_lift_l.
  Defined.

  Let leb_true:
    forall e1 e2,
    leb e1 e2 = true ->
    Le e1 e2.
  Proof.
    intros.
    functional induction leb e1 e2;
    eauto using le_leaf, le_leaf_lhs; try (inversion H; fail).
    - apply andb_true_iff in H.
      destruct H.
      eauto using le_leaf_rhs.
    - apply andb_true_iff in H.
      destruct H.
      eauto using le_node.
  Qed.

  Let leb_prop:
    forall e1 e2,
    Le e1 e2 ->
    leb e1 e2 = true.
  Proof.
    intros.
    functional induction leb e1 e2; auto;
    inversion H; try contradiction; subst.
    - rewrite andb_true_iff.
      auto.
    - rewrite andb_true_iff.
      auto.
  Qed.

  Lemma le:
    forall e1 e2,
    { Le e1 e2 } + { ~ Le e1 e2 }.
  Proof.
    intros.
    remember (leb e1 e2).
    symmetry in Heqb.
    destruct b; auto.
    right.
    unfold not; intros N.
    apply leb_prop in N.
    rewrite Heqb in *.
    inversion N.
  Defined.
End Defs.
End LE_IMPL.

Module ITC_LE2.

  (** 
    A simpler formulation of the LEQ operator; only uses two branches,
    instead of four.
  *)

Section Defs.
  Import ITC_LE.

  Definition lift_left e :=
  match e with
  | Leaf n => Leaf n
  | Node n l r => lift l n
  end.

  Definition lift_right e :=
  match e with
  | Leaf n => Leaf n
  | Node n l r => lift r n
  end.

  Inductive Le2 : event -> event -> Prop :=
  | le2_leaf:
    forall n e,
    n <= (value e) ->
    Le2 (Leaf n) e
  | le2_node:
    forall n l r e,
    n <= value e ->
    Le2 (lift l n) (lift_left e) ->
    Le2 (lift r n) (lift_right e) ->
    Le2 (Node n l r) e.

  Lemma le2_to_le:
    forall e1 e2,
    Le e1 e2 ->
    Le2 e1 e2.
  Proof.
    intros.
    induction H; auto using le2_leaf, le2_node.
  Qed.

  Lemma le_to_le2:
    forall e1 e2,
    Le2 e1 e2 ->
    Le e1 e2.
  Proof.
    intros.
    induction H.
    - destruct e.
      + auto using le_leaf.
      + simpl in *.
        auto using le_leaf_lhs.
    - destruct e.
      + auto using le_leaf_rhs.
      + auto using le_node.
  Qed.

  Lemma le2_inv_value:
    forall e1 e2,
    Le2 e1 e2 ->
    value e1 <= value e2.
  Proof.
    intros.
    inversion H; subst; simpl; auto.
  Qed.

  Lemma value_lift_rw:
    forall e n,
    value (lift e n) = value e + n.
  Proof.
    induction e; intros; simpl; auto.
  Qed.

End Defs.
End ITC_LE2.

Module ITC_LEQ2.

  (**
    A simpler definition of LEQ that a) does not alter the tree while
    traversing it, and b) only has two branches, instead of four.
    *)

Section Defs.

  Import ITC_LEQ.

  Definition lift e :=
  match e with
  | Leaf _ => 0
  | Node n _ _ => n
  end.

  Inductive Leq2 : nat ->  event -> nat -> event -> Prop :=
  | leq2_leaf:
    forall n a b e,
    n + a <= value e + b ->
    Leq2 a (Leaf n) b e
  | leq2_node:
    forall n l r a b e,
    n + a <= value e + b ->
    Leq2 (n + a) l (lift e + b) (left e) ->
    Leq2 (n + a) r (lift e + b) (right e) ->
    Leq2 a (Node n l r) b e.

  Lemma leq_to_leq2:
    forall a b e1 e2,
    Leq a e1 b e2 ->
    Leq2 a e1 b e2.
  Proof.
    intros.
    induction H; simpl; auto using leq2_leaf, leq2_node.
  Qed.

  Lemma leq2_to_leq:
    forall a b e1 e2,
    Leq2 a e1 b e2 ->
    Leq a e1 b e2.
  Proof.
    intros.
    induction H; simpl; destruct e;
    auto using leq_leaf, leq_leaf_lhs, leq_leaf_rhs, leq_node.
  Qed.

  Lemma leq2_spec:
    forall a b e1 e2,
    Leq a e1 b e2 <-> Leq2 a e1 b e2.
  Proof.
    split; eauto using leq2_to_leq, leq_to_leq2.
  Qed.

  Inductive Eq : nat -> event -> event -> Prop :=
  | eq_leaf_leaf:
    forall n m,
    Eq m (Leaf n) (Leaf n)
  | eq_leaf_node:
    forall m n l r,
    Leq2 m (Node n l r) m (Leaf n) ->
    Eq m (Leaf n) (Node n l r)
  | eq_node_leaf:
    forall n m l r,
    Leq2 m (Node n l r) m (Leaf n) ->
    Eq m (Node n l r) (Leaf n)
  | eq_node:
    forall n m l1 l2 r1 r2,
    Eq (n + m) l1 l2 ->
    Eq (n + m) r1 r2 ->
    Eq m (Node n l1 r1) (Node n l2 r2).

  Lemma leq_to_eq:
    forall e1 e2 n,
    Leq2 n e1 n e2 ->
    Leq2 n e2 n e1 ->
    Eq n e1 e2.
  Proof.
    induction e1, e2; intros;
    inversion H; inversion H0; subst; clear H H0;
    simpl in *; assert (n = n0) by auto with *; subst.
    - auto using eq_leaf_leaf.
    - auto using eq_leaf_node, leq2_node.
    - auto using eq_node_leaf, leq2_node.
    - eauto using eq_node.
  Qed.

  Lemma eq_to_leq:
    forall e1 e2 n,
    Eq n e1 e2 ->
    Leq2 n e1 n e2.
  Proof.
    intros.
    induction H; auto using leq2_leaf.
    auto using leq2_node.
  Qed.

  Lemma eq_symm:
    forall n e1 e2,
    Eq n e1 e2 ->
    Eq n e2 e1.
  Proof.
    intros.
    induction H;
    auto using eq_leaf_leaf, eq_node_leaf, eq_leaf_node, eq_node.
  Qed.

  Lemma eq_spec:
    forall n e1 e2,
    Eq n e1 e2 <-> Leq2 n e1 n e2 /\ Leq2 n e2 n e1.
  Proof.
    split; intros.
    - split; auto using eq_to_leq, eq_symm.
    - destruct H; auto using leq_to_eq.
  Qed.

  Section Merge.
    Import Datatypes.
  Definition merge (c1 c2:comparison) :=
  match c1, c2 with
  | Lt, Lt | Eq, Lt | Lt, Eq => Some Lt
  | Gt, Gt | Eq, Gt | Gt, Eq => Some Gt
  | Eq, Eq => Some Eq
  | Lt, Gt | Gt, Lt => None
  end.
  End Merge.

  Inductive Cmp0 n e1 e2 : comparison -> Prop :=
  | cmp0_eq:
    Eq n e1 e2 ->
    Cmp0 n e1 e2 Datatypes.Eq
  | cmp0_lt:
    Leq2 n e1 n e2 ->
    ~ Leq2 n e2 n e1 ->
    Cmp0 n e1 e2 Lt
  | cmp0_gt:
    Cmp0 n e2 e1 Lt ->
    Cmp0 n e1 e2 Gt.

  Let cmp_fst (n1 n2:nat) c :=
  match c with
  | Gt => n2
  | Lt => n1
  | Datatypes.Eq => n1
  end.

  Inductive IsLeaf: event -> Prop :=
  | is_leaf_def:
    forall n,
    IsLeaf (Leaf n).

  Inductive Ordered : comparison -> Prop :=
  | ordered_lt: Ordered Lt
  | ordered_gt: Ordered Gt.

  Inductive Cmp : nat -> event -> event -> comparison -> Prop :=
  | cmp_leaf:
    forall e1 e2 m c,
    Cmp0 m e1 e2 c ->
    IsLeaf e1 \/ IsLeaf e2 \/ Ordered (nat_compare (value e1) (value e2))->
    Cmp m e1 e2 c
  | cmp_node:
    forall n m l1 l2 r1 r2 c1 c2 c3,
    Cmp (n + m) l1 l2 c1 ->
    Cmp (n + m) r1 r2 c2->
    merge c1 c2 = Some c3 ->
    Cmp m (Node n l1 r1) (Node n l2 r2) c3.

  Let merge_inv_eq:
    forall c1 c2,
    merge c1 c2 = Some Datatypes.Eq ->
    c1 = Datatypes.Eq /\ c2 = Datatypes.Eq.
  Proof.
    intros.
    destruct c1, c2; inversion H; auto.
  Qed.

  Let nat_compare_rev:
    forall n1 n2 c,
    nat_compare n1 n2 = c ->
    nat_compare n2 n1 = (CompOpp c).
  Proof.
    intros.
    destruct c; simpl.
    - rewrite PeanoNat.Nat.compare_eq_iff in *; auto.
    - rewrite PeanoNat.Nat.compare_lt_iff, PeanoNat.Nat.compare_gt_iff in *; auto.
    - rewrite PeanoNat.Nat.compare_lt_iff, PeanoNat.Nat.compare_gt_iff in *; auto.
  Qed.

  Let cmp0_symm:
    forall n e1 e2 c,
    Cmp0 n e1 e2 c ->
    Cmp0 n e2 e1 (CompOpp c).
  Proof.
    intros.
    inversion H; simpl; clear H; subst.
    - apply cmp0_eq; auto using eq_symm.
    - apply cmp0_gt; auto using cmp0_lt.
    - assumption.
  Qed.

  Let rev_comp_merge:
    forall c1 c2 c3,
    merge c1 c2 = Some c3 ->
    merge (CompOpp c1) (CompOpp c2) = Some (CompOpp c3).
  Proof.
    intros.
    destruct c1, c2, c3; simpl; inversion H; auto.
  Qed.

  Let ordered_rev:
    forall c,
    Ordered c ->
    Ordered (CompOpp c).
  Proof.
    intros.
    inversion H; simpl; subst; auto using ordered_gt, ordered_lt.
  Qed.


  Let rev_comp_inv_eq_lt:
    forall c,
    Lt = CompOpp c ->
    c = Gt.
  Proof.
    intros.
    destruct c; auto.
    inversion H.
  Qed.

  Let rev_comp_inv_eq_gt:
    forall c,
    Gt = CompOpp c ->
    c = Lt.
  Proof.
    intros.
    destruct c; auto.
    inversion H.
  Qed.

  Lemma cmp_symm:
    forall e2 e1 n c,
    Cmp n e1 e2 c ->
    Cmp n e2 e1 (CompOpp c).
  Proof.
    intros.
    induction H. {
      apply cmp0_symm in H.
      destruct H0 as [?|[?|?]]; inversion H0; subst; simpl;
      auto using cmp_leaf, is_leaf_def.
      - apply cmp_leaf; auto using is_leaf_def, ordered_rev.
        erewrite nat_compare_rev in H2; eauto.
        apply rev_comp_inv_eq_lt in H2.
        rewrite H2.
        auto using ordered_gt.
      - apply cmp_leaf; auto.
        erewrite nat_compare_rev in H2; eauto.
        apply rev_comp_inv_eq_gt in H2.
        rewrite H2 in *.
        auto using ordered_lt.
    }
    apply rev_comp_merge in H1.
    eauto using cmp_node.
  Qed.

  Lemma leq2_inv_value:
    forall a b e1 e2,
    Leq2 a e1 b e2 ->
    value e1 + a <= value e2 + b.
  Proof.
    intros.
    inversion H; subst; auto.
  Qed.

  Lemma cmp0_inv_eq_value:
    forall m e1 e2,
    Cmp0 m e1 e2 Datatypes.Eq ->
    value e1 = value e2.
  Proof.
    intros.
    inversion H.
    apply eq_spec in H0.
    destruct H0.
    apply leq2_inv_value in H0.
    apply leq2_inv_value in H1.
    auto with *.
  Qed.

  Require Import Coq.omega.Omega.

  Lemma cmp_to_eq:
    forall e1 e2 n,
    Cmp n e1 e2 Datatypes.Eq ->
    Eq n e1 e2.
  Proof.
    induction e1; intros. {
      inversion H; subst; clear H.
      inversion H0.
      auto using leq_to_eq.
    }
    inversion H; subst; clear H. {
      destruct H1 as [Hx|[Hx|Hx]]; inversion Hx; subst; clear Hx.
      inversion H0; auto.
      + apply cmp0_inv_eq_value in H0.
        simpl in *.
        subst.
        symmetry in H1.
        rewrite PeanoNat.Nat.compare_lt_iff in H1.
        omega.
      + apply cmp0_inv_eq_value in H0.
        simpl in *.
        subst.
        symmetry in H1.
        rewrite PeanoNat.Nat.compare_gt_iff in H1.
        omega.
    }
    apply merge_inv_eq in H8; destruct H8; subst.
    eauto using eq_node.
  Qed.

  Let cmp_node_eq:
    forall n m l1 l2 r1 r2,
    Cmp (n + m) l1 l2 Datatypes.Eq ->
    Cmp (n + m) r1 r2 Datatypes.Eq ->
    Cmp m (Node n l1 r1) (Node n l2 r2) Datatypes.Eq.
  Proof.
    intros.
    assert (R1: cmp_fst n n (PeanoNat.Nat.compare n n) = n). {
      rewrite PeanoNat.Nat.compare_refl in *; subst.
      auto.
    }
    eapply cmp_node with (c1:=Datatypes.Eq) (c2:=Datatypes.Eq) (c3:=Datatypes.Eq);
    simpl; try rewrite R1;
    eauto.
  Qed.

  Let cmp_leaf_leaf_eq:
    forall m n,
    Cmp m (Leaf n) (Leaf n) Datatypes.Eq.
  Proof.
    auto using is_leaf_def, cmp0_eq,
    PeanoNat.Nat.compare_refl, eq_leaf_leaf, cmp_leaf.
  Qed.

  Let eq_to_cmp:
    forall e1 e2 n,
    Eq n e1 e2 ->
    Cmp n e1 e2 Datatypes.Eq.
  Proof.
    intros.
    induction H;
    auto using cmp_leaf, is_leaf_def, cmp0_eq,
    leq_to_eq, leq2_leaf, PeanoNat.Nat.compare_refl.
  Qed.

  Let merge_inv_lt:
    forall c1 c2,
    merge c1 c2 = Some Lt ->
    (c1 = Lt /\ (c2 = Lt \/ c2 = Datatypes.Eq)) \/ (c2 = Lt /\ (c1 = Lt \/ c1 = Datatypes.Eq)).
  Proof.
    intros.
    destruct c1, c2; simpl in *; inversion H;
    try (right; split; intuition; inversion H0; fail);
    left; split; intuition; inversion H0.
  Qed.

  Let merge_inv_gt:
    forall c1 c2,
    merge c1 c2 = Some Gt ->
    (c1 = Gt /\ (c2 = Gt \/ c2 = Datatypes.Eq)) \/ (c2 = Gt /\ (c1 = Gt \/ c1 = Datatypes.Eq)).
  Proof.
    intros.
    destruct c1, c2; simpl in *; inversion H;
    try (right; split; intuition; inversion H0; fail);
    left; split; intuition; inversion H0.
  Qed.

  Let cmp_to_leq2:
    forall e1 e2 n,
    Cmp n e1 e2 Datatypes.Lt ->
    Leq2 n e1 n e2.
  Proof.
    induction e1; intros. {
      inversion H; subst; clear H.
      inversion H0; auto.
    }
    inversion H; subst; clear H. {
      inversion H0.
      auto.
    }
    apply merge_inv_lt in H8.
    destruct H8 as [(?,?)|(?,?)]; subst. {
      destruct H0; subst.
      - apply leq2_node; simpl; auto with *.
      - apply cmp_to_eq in H7.
        apply eq_spec in H7.
        destruct H7.
        apply leq2_node; simpl; auto with *.
    }
    destruct H0; subst.
    - apply leq2_node; simpl; auto with *.
    - apply cmp_to_eq in H4.
      apply eq_spec in H4.
      destruct H4.
      apply leq2_node; simpl; auto with *.
  Qed.

  Lemma cmp_gt_to_leq2:
    forall e1 e2 n,
    Cmp n e1 e2 Datatypes.Gt ->
    Leq2 n e2 n e1.
  Proof.
    induction e1; intros. {
      inversion H; subst; clear H.
      inversion H0; subst; clear H0.
      inversion H; auto.
    }
    inversion H; subst; clear H. {
      inversion H0; subst; clear H0.
      inversion H; subst; clear H.
      auto.
    }
    apply merge_inv_gt in H8.
    destruct H8 as [(?,?)|(?,?)]; subst. {
      destruct H0; subst.
      - apply leq2_node; simpl; auto with *.
      - apply cmp_to_eq in H7.
        apply eq_spec in H7.
        destruct H7.
        apply leq2_node; simpl; auto with *.
    }
    destruct H0; subst.
    - apply leq2_node; simpl; auto with *.
    - apply cmp_to_eq in H4.
      apply eq_spec in H4.
      destruct H4.
      apply leq2_node; simpl; auto with *.
  Qed.

  Let lt_eq_to_leq2:
    forall e1 e2 n,
    Cmp n e1 e2 Datatypes.Lt \/ Cmp n e1 e2 Datatypes.Eq ->
    Leq2 n e1 n e2.
  Proof.
    intros.
    destruct H.
    - auto.
    - apply cmp_to_eq in H.
      apply eq_spec in H.
      destruct H.
      assumption.
  Qed.

  Let gt_eq_to_leq2:
    forall e1 e2 n,
    Cmp n e1 e2 Datatypes.Gt \/ Cmp n e1 e2 Datatypes.Eq ->
    Leq2 n e2 n e1.
  Proof.
    intros.
    destruct H.
    - auto using cmp_gt_to_leq2.
    - apply cmp_to_eq in H.
      apply eq_spec in H.
      destruct H.
      assumption.
  Qed.

  Lemma eq_inv_value:
    forall n e1 e2,
    Eq n e1 e2 ->
    value e1 = value e2.
  Proof.
    intros.
    inversion H; subst; auto.
  Qed.

  Lemma cmp0_inv_lt_value:
    forall m e1 e2,
    Cmp0 m e1 e2 Lt ->
    value e1 <= value e2.
  Proof.
    intros.
    inversion H.
    apply leq2_inv_value in H0.
    auto with *.
  Qed.

  Let cmp0_inv_gt_value:
    forall m e1 e2,
    Cmp0 m e1 e2 Gt ->
    value e1 >= value e2.
  Proof.
    intros.
    inversion H.
    apply cmp0_inv_lt_value in H0.
    auto.
  Qed.

  Lemma cmp0_to_gt:
    forall m e1 e2 c,
    value e1 > value e2 ->
    Cmp0 m e1 e2 c ->
    c = Gt.
  Proof.
    intros.
    inversion H0; subst.
    - apply eq_inv_value in H1.
      omega.
    - apply cmp0_inv_lt_value in H0.
      omega.
    - trivial.
  Qed.

  Lemma cmp0_to_lt:
    forall m e1 e2 c,
    value e1 < value e2 ->
    Cmp0 m e1 e2 c ->
    c = Lt.
  Proof.
    intros.
    inversion H0; subst.
    - apply eq_inv_value in H1.
      omega.
    - trivial.
    - apply cmp0_inv_lt_value in H1.
      simpl in *.
      omega.
  Qed.

  Lemma cmp0_lt_0:
    forall m e1 e2,
    value e1 < value e2 ->
    Leq2 m e1 m e2 ->
    Cmp0 m e1 e2 Lt.
  Proof.
    intros.
    apply cmp0_lt; auto.
    unfold not; intros N.
    apply leq2_inv_value in N.
    omega.
  Qed.

  Lemma cmp0_gt_0:
    forall m e1 e2,
    value e1 > value e2 ->
    Leq2 m e2 m e1 ->
    Cmp0 m e1 e2 Gt.
  Proof.
    intros.
    apply cmp0_gt.
    auto using cmp0_lt_0.
  Qed.

  Lemma cmp0_lt_1:
    forall m n e,
    n = value e ->
    ~ Leq2 m e m (Leaf n) ->
    Cmp0 m (Leaf n) e Datatypes.Lt.
  Proof.
    intros.
    apply cmp0_lt; auto using leq2_leaf with *.
  Qed.

End Defs.

End ITC_LEQ2.

Module EQ_IMPL.
  Section Defs.
  Import ITC_LEQ2.
  Require Import Coq.Arith.Peano_dec.

  Fixpoint itc_leq a e1 b e2 :=
  let n_a := value e1 + a in
  if le_dec n_a (value e2 + b) then
    match e1 with
    | Leaf _ => true
    | Node n l r =>
      let e_b := lift e2 + b in
      itc_leq n_a l e_b (left e2) && itc_leq n_a r e_b (right e2)
    end
  else false.

  Fixpoint itc_eq m e1 e2 :=
  let n := value e1 in
  if eq_nat_dec n (value e2) then
    match e1 with
    | Leaf _ =>
      match e2 with
      | Leaf _ => true
      | Node _ _ _ => itc_leq m e2 m e1
      end
    | Node _ l1 r1 =>
      match e2 with
      | Leaf _ => itc_leq m e1 m e2
      | Node _ l2 r2 =>
        itc_eq (n + m) l1 l2 && itc_eq (n + m) r1 r2
      end
    end 
  else false.

  Lemma itc_leq_true:
    forall e1 a b e2,
    itc_leq a e1 b e2 = true ->
    Leq2 a e1 b e2.
  Proof.
    induction e1; intros. {
      simpl in *.
      destruct (le_dec (n + a) (value e2 + b)). {
        auto using leq2_leaf.
      }
      inversion H.
    }
    simpl in *.
    destruct (le_dec (n + a) (value e2 + b)). {
      rewrite andb_true_iff in H.
      destruct H.
      auto using leq2_node.
    }
    inversion H.
  Qed.

  Lemma itc_leq_prop:
    forall e1 a b e2,
    Leq2 a e1 b e2 ->
    itc_leq a e1 b e2 = true.
  Proof.
    intros.
    induction H.
    - simpl.
      destruct (le_dec (n+ a) (value e + b)). {
        trivial.
      }
      contradiction.
    - simpl.
      destruct (le_dec (n+ a) (value e + b)). {
        rewrite IHLeq2_1 in *.
        rewrite IHLeq2_2 in *.
        auto.
      }
      contradiction.
  Qed.

  Lemma itc_eq_true:
    forall e1 e2 m,
    itc_eq m e1 e2 = true ->
    Eq m e1 e2.
  Proof.
    induction e1; intros; simpl in *;
    destruct (eq_nat_dec n (value e2)); subst; inversion H; clear H.
    - destruct e2; simpl. {
        auto using eq_leaf_leaf.
      }
      auto using eq_leaf_node, itc_leq_true.
    - destruct e2; simpl in *. {
        auto using eq_node_leaf, itc_leq_true.
      }
      rewrite andb_true_iff in *.
      destruct H1.
      auto using eq_node.
  Qed.

  Lemma itc_leq_false:
    forall a e1 b e2,
    itc_leq a e1 b e2 = false ->
    ~ Leq2 a e1 b e2.
  Proof.
    intros; unfold not; intros n.
    apply itc_leq_prop in n.
    rewrite H in *.
    inversion n.
  Qed.

  Lemma itc_leq_not_prop:
    forall a e1 b e2,
    ~ Leq2 a e1 b e2 ->
    itc_leq a e1 b e2 = false.
  Proof.
    intros.
    remember (itc_leq a e1 b e2).
    symmetry in Heqb0.
    destruct b0. {
      apply itc_leq_true in Heqb0.
      contradiction.
    }
    trivial.
  Qed.

  Lemma itc_eq_prop:
    forall e1 e2 m,
    Eq m e1 e2 ->
    itc_eq m e1 e2 = true.
  Proof.
    intros.
    induction H; simpl; destruct (eq_nat_dec n n);
    simpl; auto; try intuition.
    - apply itc_leq_prop in H.
      simpl in *.
      rewrite <- H.
      trivial.
    - apply itc_leq_prop in H.
      simpl in *; rewrite <- H.
      auto.
  Qed.

  Definition itc_eq_dec m e1 e2:
    { Eq m e1 e2 } + { ~ Eq m e1 e2 }.
  Proof.
    intros.
    remember (itc_eq m e1 e2).
    destruct b. {
      symmetry in Heqb.
      auto using itc_eq_true.
    }
    right; intros N.
    apply itc_eq_prop in N.
    rewrite N in *.
    inversion Heqb.
  Defined.

  Definition cmp0 n e1 e2 :=
  match itc_leq n e1 n e2, itc_leq n e2 n e1 with
  | true, true => Some Datatypes.Eq
  | true, false => Some Lt
  | false, true => Some Gt
  | false, false => None
  end.

  Lemma cmp0_some:
    forall n e1 e2 c,
    cmp0 n e1 e2 = Some c ->
    Cmp0 n e1 e2 c.
  Proof.
    unfold cmp0; intros.
    remember (itc_leq _ e1 _ _).
    remember (itc_leq _ e2 _ _).
    symmetry in Heqb.
    symmetry in Heqb0.
    destruct b, b0; inversion H; subst;
    try apply itc_leq_true in Heqb;
    try apply itc_leq_true in Heqb0;
    auto using cmp0_eq, leq_to_eq.
    + apply itc_leq_false in Heqb0.
      auto using cmp0_lt.
    + apply itc_leq_false in Heqb.
      auto using cmp0_lt, cmp0_gt.
  Qed.

  Fixpoint itc_cmp m e1 e2 :=
  let n := value e1 in
  let do_cmp0 := cmp0 m e1 e2 in
  match e1 with
  | Leaf _ => do_cmp0
  | Node _ l1 r1 =>
    match e2 with
    | Leaf _ => do_cmp0
    | Node _ l2 r2 =>
      match nat_compare n (value e2) with
      | Datatypes.Eq =>
        let c1 := itc_cmp (n + m) l1 l2 in
        let c2 := itc_cmp (n + m) r1 r2 in
        match c1, c2 with
        | Some c1, Some c2 => merge c1 c2
        | _,_ => None
        end
      | _ => do_cmp0
      end
    end
  end.


  Lemma itc_cmp_some:
    forall e1 e2 c m,
    itc_cmp m e1 e2 = Some c ->
    Cmp m e1 e2 c.
  Proof.
    induction e1; intros. {
      simpl in *.
      apply cmp_leaf; auto using is_leaf_def, cmp0_some.
    }
    destruct e2; simpl in *. {
      auto using cmp0_some, is_leaf_def, cmp_leaf.
    }
    remember (nat_compare n n0).
    symmetry in Heqc0.
    destruct c0.
    - apply nat_compare_eq in Heqc0; subst.
      remember (itc_cmp _ _ e2_1).
      remember (itc_cmp _ _ e2_2).
      destruct o. {
        destruct o0. {
          symmetry in Heqo.
          symmetry in Heqo0.
          eauto using cmp_node.
        }
        inversion H.
      }
      inversion H.
    - assert (Hx:=H).
      apply cmp0_some in H.
      simpl in *.
      assert (c = Lt). {
        rewrite PeanoNat.Nat.compare_lt_iff in Heqc0.
        eapply cmp0_to_lt in H; auto.
      }
      subst.
      apply cmp_leaf; auto using is_leaf_def.
      simpl.
      rewrite Heqc0.
      auto using ordered_lt.
    - assert (Hx:=H).
      apply cmp0_some in H.
      assert (c = Gt). {
        rewrite PeanoNat.Nat.compare_gt_iff in Heqc0.
        eapply cmp0_to_gt in H; auto.
      }
      subst.
      apply cmp_leaf; auto using is_leaf_def.
      simpl.
      rewrite Heqc0.
      auto using ordered_gt.
  Qed.

  Let cmp0_prop:
    forall e1 e2 n c,
    Cmp0 n e1 e2 c ->
    cmp0 n e1 e2 = Some c.
  Proof.
    intros.
    unfold cmp0; intros.
    inversion H; clear H.
    - apply eq_spec in H0.
      destruct H0.
      apply itc_leq_prop in H0.
      rewrite H0.
      apply itc_leq_prop in H.
      rewrite H.
      trivial.
    - apply itc_leq_prop in H0.
      rewrite H0; subst.
      apply itc_leq_not_prop in H1.
      rewrite H1.
      trivial.
    - inversion H0.
      apply itc_leq_prop in H.
      rewrite H; subst.
      apply itc_leq_not_prop in H2.
      rewrite H2.
      trivial.
  Qed.

  Let leq_nleq_to_cmp0_lt:
    forall e1 e2 n c,
    Leq2 n e1 n e2 ->
    ~ Leq2 n e2 n e1 ->
    Cmp0 n e1 e2 c ->
    c = Lt.
  Proof.
    intros.
    inversion H1; subst; clear H1.
    - contradiction H0.
      auto using eq_symm, eq_to_leq.
    - trivial.
    - inversion H2.
      contradiction.
  Qed.

  Definition comp_eq:
    forall (x y:comparison),
    { x = y } + { x <> y }.
  Proof.
    intros.
    destruct x, y; auto; right; unfold not; intros N; inversion N.
  Defined.

  Let merge_inv_lt:
    forall c1 c2,
    merge Lt c1 = Some c2 ->
    c2 = Lt.
  Proof.
    intros.
    destruct c1; simpl in *; inversion H; auto.
  Qed.

  Let merge_symm:
    forall c1 c2,
    merge c1 c2 = merge c2 c1.
  Proof.
    intros; destruct c1, c2; auto.
  Qed.

  Let merge_inv_gt:
    forall c1 c2,
    merge Gt c1 = Some c2 ->
    c2 = Gt.
  Proof.
    intros.
    destruct c1; simpl in *; inversion H; auto.
  Qed.

  Require Import Coq.omega.Omega.

  Lemma itc_cmp_prop:
    forall e1 e2 c m,
    Cmp m e1 e2 c ->
    itc_cmp m e1 e2 = Some c.
  Proof.
    induction e1; intros. {
      inversion H; subst; clear H.
      simpl.
      auto using cmp0_prop.
    }
    simpl.
    inversion H; subst; clear H. {
      destruct H1 as [Hx|[Hx|Hx]].
      - inversion Hx.
      - inversion Hx; subst.
        auto using cmp0_prop.
      - destruct e2.
        + auto using cmp0_prop.
        + simpl in *.
          remember (n ?= n0).
          symmetry in Heqc0.
          destruct c0; inversion Hx; auto using cmp0_prop.
    }
    simpl.
    rewrite Nat.compare_refl.
    apply IHe1_1 in H4.
    apply IHe1_2 in H7.
    rewrite H4.
    rewrite H7.
    trivial.
  Qed.

  Definition itc_leq_dec a e1 b e2:
    { Leq2 a e1 b e2 } + { ~ Leq2 a e1 b e2 }.
  Proof.
    remember (itc_leq a e1 b e2).
    destruct b0. {
      symmetry in Heqb0.
      auto using itc_leq_true.
    }
    right; unfold not; intros N.
    apply itc_leq_prop in N.
    rewrite N in *.
    inversion Heqb0.
  Defined.

  Definition itc_cmp_dec m e1 e2:
    { c : comparison | Cmp m e1 e2 c } + { _ : unit | forall c, ~ Cmp m e1 e2 c }.
  Proof.
    remember (itc_cmp m e1 e2).
    symmetry in Heqo.
    destruct o. {
      eauto using exist, itc_cmp_some.
    }
    right.
    apply exist; auto using tt.
    intros; unfold not; intros N.
    apply itc_cmp_prop in N.
    rewrite N in *.
    inversion Heqo.
  Defined.

  Let merge_inv_none:
    forall c1 c2,
    merge c1 c2 = None ->
    (c1 = Lt /\ c2 = Gt) \/ (c1 = Gt /\ c2 = Lt).
  Proof.
    intros; destruct c1, c2; inversion H; auto.
  Qed.

  Lemma cmp0_none:
    forall e1 n e2,
    cmp0 n e1 e2 = None ->
    ~ Leq2 n e1 n e2 /\ ~ Leq2 n e2 n e1.
  Proof.
    unfold cmp0.
    intros.
    remember (itc_leq _ e1 _ _).
    remember (itc_leq _ e2 _ _).
    symmetry in Heqb.
    symmetry in Heqb0.
    destruct b. {
      destruct b0. {
        inversion H.
      }
      inversion H.
    }
    destruct b0. {
      inversion H.
    }
    auto using itc_leq_false.
  Qed.

  Lemma eq_to_cmp_eq:
    forall e1 n e2,
    Eq n e1 e2 ->
    Cmp n e1 e2 Datatypes.Eq.
  Proof.
    induction e1; intros. {
      auto using cmp_leaf, cmp0_eq, is_leaf_def.
    }
    destruct e2. {
      auto using cmp_leaf, cmp0_eq, is_leaf_def.
    }
    inversion H; subst; clear H.
    eauto using cmp_node.
  Qed.

  Lemma leq_to_cmp_lt:
    forall e1 e2 n,
    Leq2 n e1 n e2 ->
    ~ Leq2 n e2 n e1 ->
    Cmp n e1 e2 Lt.
  Proof.
    induction e1; intros. {
      simpl in *.
      auto using cmp_leaf, is_leaf_def, cmp0_lt.
    }
    simpl in *.
    remember (nat_compare (value (Node n e1_1 e1_2)) (value e2)).
    symmetry in Heqc.
    destruct e2. {
      auto using cmp_leaf, is_leaf_def, cmp0_lt.
    }
    destruct c.
    - simpl in *.
      apply Nat.compare_eq in Heqc; subst.
      inversion H; subst; clear H.
      simpl in *.
      apply itc_leq_not_prop in H0.
      simpl in *.
      destruct (le_dec (n1+n0) (n1+n0)). {
        rewrite andb_false_iff in *.
        destruct H0;
        apply itc_leq_false in H. {
          destruct (itc_leq_dec (n1+n0) e2_2 (n1+n0) e1_2). {
            eauto using cmp_node, eq_to_cmp_eq, leq_to_eq.
          }
          eauto using cmp_node.
        }
        destruct (itc_leq_dec (n1+n0) e2_1 (n1+n0) e1_1). {
          eauto using cmp_node, eq_to_cmp_eq, leq_to_eq.
        }
        eauto using cmp_node.
      }
      contradiction.
    - apply cmp_leaf; auto using cmp0_lt.
      rewrite Heqc.
      auto using ordered_lt.
    - apply leq2_inv_value in H.
      apply nat_compare_gt in Heqc.
      omega.
  Qed.

  Lemma cmp_none:
    forall e1 e2 n,
    itc_cmp n e1 e2 = None ->
    forall c, ~ Cmp n e1 e2 c.
  Proof.
    intros; unfold not; intros N.
    apply itc_cmp_prop in N.
    rewrite N in *.
    inversion H.
  Qed.

  Let cmp_n:
    forall n e1 e2,
    (forall c, ~ Cmp n e1 e2 c) ->
    ~ Leq2 n e1 n e2 /\ ~ Leq2 n e2 n e1.
  Proof.
    intros.
    destruct (itc_leq_dec n e1 n e2). {
      destruct (itc_leq_dec n e2 n e1). {
        assert (Hx: Eq n e1 e2) by auto using leq_to_eq.
        contradiction (H Datatypes.Eq).
        auto using eq_to_cmp_eq.
      }
      contradiction (H Lt).
      auto using leq_to_cmp_lt.
    }
    destruct (itc_leq_dec n e2 n e1). {
      contradiction (H Gt).
      assert (Cmp n e1 e2 (CompOpp Lt)). {
        apply cmp_symm.
        auto using leq_to_cmp_lt.
      }
      simpl in *.
      auto.
    }
    auto.
  Qed.

  Lemma cmp_none_alt:
    forall e1 e2 n,
    itc_cmp n e1 e2 = None ->
    ~ Leq2 n e1 n e2 /\ ~ Leq2 n e2 n e1.
  Proof.
    intros.
    auto using cmp_none.
  Qed.

  Definition cmp0_fast n e1 e2 :=
  match nat_compare (value e1) (value e2) with
  | Lt => if itc_leq n e1 n e2 then Some Lt else None
  | Gt => if itc_leq n e2 n e1 then Some Gt else None
  | Eq => cmp0 n e1 e2
  end.

  Let cmp0_fast_0:
    forall n e1 e2,
    cmp0_fast n e1 e2 = cmp0 n e1 e2.
  Proof.
    intros.
    unfold cmp0_fast.
    unfold cmp0.
    remember (itc_leq n e1 n e2).
    remember (itc_leq n e2 n e1).
    symmetry in Heqb0.
    symmetry in Heqb.
    destruct b. {
      apply itc_leq_true in Heqb.
      destruct b0. {
        apply itc_leq_true in Heqb0.
        assert (Hx: value e1 = value e2). {
          apply leq2_inv_value in Heqb.
          apply leq2_inv_value in Heqb0.
          auto with *.
        }
        rewrite Hx.
        rewrite Nat.compare_refl.
        trivial.
      }
      apply leq2_inv_value in Heqb.
      apply le_lt_or_eq in Heqb.
      destruct Heqb.
      - assert (Hy : value e1 < value e2) by auto with *.
        apply nat_compare_lt in Hy.
        rewrite Hy.
        trivial.
      - assert (Hy : value e1 = value e2) by auto with *.
        rewrite Hy in *.
        rewrite Nat.compare_refl.
        trivial.
    }
    destruct b0. {
      apply itc_leq_true in Heqb0.
      remember (value e1 ?= value e2).
      destruct c.
      - trivial.
      - apply leq2_inv_value in Heqb0.
        symmetry in Heqc.
        apply nat_compare_lt in Heqc.
        omega.
      - trivial.
    }
    destruct (value e1 ?= value e2); auto.
  Qed.

  End Defs.

End EQ_IMPL.

