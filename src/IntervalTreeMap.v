Require Import Coq.Arith.Arith.
Require Import Coq.omega.Omega.

Section Defs.

  Inductive event :=
  | Leaf: nat -> event
  | Node: nat -> event -> event -> event.

  Definition value e :=
  match e with
  | Leaf n => n
  | Node n _ _ => n
  end.

  Definition left e :=
  match e with
  | Leaf n => Leaf n
  | Node n l r => l
  end.

  Definition right e :=
  match e with
  | Leaf n => Leaf n
  | Node n l r => r
  end.

  Definition lift e m : event :=
  match e with
  | Leaf n => Leaf (n + m)
  | Node n e1 e2 => Node (n + m) e1 e2
  end.

  Definition sink e m : event :=
  match e with
  | Leaf n => Leaf (n - m)
  | Node n e1 e2 => Node (n - m) e1 e2
  end.

  Lemma lift_rw_0:
    forall e,
    lift e 0 = e.
  Proof.
    intros.
    destruct e; intros; simpl;
    rewrite Nat.add_0_r; auto.
  Qed.

  Lemma sink_rw_0:
    forall e,
    sink e 0 = e.
  Proof.
    intros.
    destruct e; intros; simpl; rewrite Nat.sub_0_r; auto.
  Qed.

End Defs.