Require Import Coq.Arith.Arith.
Require Import IntervalTreeMap.

Section Defs.

  Inductive Min : event -> nat -> Prop :=
  | min_leaf:
    forall n,
    Min (Leaf n) n
  | min_node:
    forall n l r nl nr,
    Min l nl ->
    Min r nr ->
    Min (Node n l r) (n + (Nat.min nl nr)).

  Inductive Norm: event -> event -> Prop :=
  | norm_leaf:
    forall n,
    Norm (Leaf n) (Leaf n)
  | norm_eq:
    forall n m,
    Norm (Node n (Leaf m) (Leaf m)) (Leaf (n + m))
  | norm_neq:
    forall n l1 r1 l2 r2 m,
    Norm l1 l2 ->
    Norm r1 r2 ->
    m = Nat.min (value l2) (value r2) ->
    Norm (Node n l1 r1) (Node (n + m) (sink l2 m) (sink r2 m)).

  Inductive Normalized : event -> Prop :=
  | normalized_leaf:
    forall n,
    Normalized (Leaf n)
  | normalized_node:
    forall n l r,
    Normalized l ->
    Normalized r ->
    (value l = 0 \/ value r = 0) ->
    Normalized (Node n l r).

  Lemma normalized_to_norm:
    forall e,
    Normalized e ->
    Norm e e.
  Proof.
    intros.
    induction H. {
      auto using norm_leaf.
    }
    destruct H1. {
      assert (Norm (Node n l r) (Node (n + 0) (sink l 0) (sink r 0))). {
        apply norm_neq; try rewrite H1; auto with *.
      }
      repeat rewrite sink_rw_0 in *.
      rewrite Nat.add_0_r in *.
      assumption.
    }
    assert (Norm (Node n l r) (Node (n + 0) (sink l 0) (sink r 0))). {
      apply norm_neq; try rewrite H1; auto with *.
    }
    repeat rewrite sink_rw_0 in *.
    rewrite Nat.add_0_r in *.
    assumption.
  Qed.

  Let min_or:
    forall n m,
    Nat.min n m = 0 ->
    n = 0 \/ m = 0.
  Proof.
    intros.
    destruct (Nat.min_dec n m);
      rewrite e in *; auto.
  Qed.

  Let norm_to_normalized_0:
    forall e n,
    Norm (sink e n) (sink e n) ->
    Normalized (sink e n).
  Proof.
    induction e; intros. {
      simpl; auto using normalized_leaf.
    }
    inversion H; subst; simpl in *; clear H.
    assert (Hm: Nat.min (value l2) (value r2) = 0) by auto with *; clear H3.
    rewrite Hm in *.
    repeat rewrite sink_rw_0 in *.
    assert (Hl : Norm (sink l2 0) (sink l2 0)). {
      repeat rewrite sink_rw_0; auto.
    }
    assert (Hr : Norm (sink r2 0) (sink r2 0)). {
      repeat rewrite sink_rw_0; auto.
    }
    apply IHe1 in Hl.
    apply IHe2 in Hr.
    repeat rewrite sink_rw_0 in *.
    apply normalized_node; auto with *.
  Qed.

  Lemma norm_to_normalized:
    forall e,
    Norm e e ->
    Normalized e.
  Proof.
    intros.
    assert (Hr: Norm (sink e 0) (sink e 0)). {
      repeat rewrite sink_rw_0; auto.
    }
    apply norm_to_normalized_0 in Hr.
    rewrite sink_rw_0 in *.
    assumption.
  Qed.

  Let normalized_inv_l:
    forall l n r,
    Normalized (Node n l r) ->
    Normalized l.
  Proof.
    intros.
    inversion H.
    auto.
  Qed.

  Lemma normalized_min:
    forall e,
    Normalized e ->
    Min e (value e).
  Proof.
    induction e; intros. {
      inversion H; simpl; auto using min_leaf.
    }
    inversion H; subst; clear H.
    apply IHe1 in H3.
    apply IHe2 in H4.
    assert (Min (Node n e1 e2) (n + Nat.min (value e1) (value e2))). {
      apply min_node; auto.
    }
    destruct H5 as [R|R];
      simpl;
      rewrite R in *;
      try rewrite Nat.min_0_l in *;
      try rewrite Nat.min_0_r in *;
      rewrite Nat.add_0_r in *;
      assumption.
  Qed.

  Fixpoint norm e: event :=
  match e with
  | Leaf _ => e
  | Node n (Leaf l) (Leaf r) =>
    if Nat.eq_dec l r then Leaf (n + l)
    else
    let m := Nat.min l r in
    Node (n + m) (Leaf (l-m)) (Leaf (r-m))
  | Node n l r =>
    let l := norm l in
    let r := norm r in
    let m := Nat.min (value l) (value r) in
    Node (n + m) (sink l m) (sink r m)
  end.

