Set Implicit Arguments.

Require Import Coq.Classes.RelationClasses.

Section CausalityRelation.
  Section Defs.
  Variable A:Type.
  Variable E:(A * A) -> Prop.
  Variable lt: A -> A -> Prop.
  Context {S: StrictOrder lt}.
  Class CausalityRelation := {
    causality_1:
      forall x y,
      E (x, y) ->
      lt x y;
    causality_2:
      forall x y,
      lt x y ->
      E (x, y);
  }.
  End Defs.
End CausalityRelation.

Section Clock.
  (** We define a simple language to embed logical clock representations of
  type C [C]. *)

  Variable C:Type.

  (** The language [history] describes the history of operations that produced
  a certain clock [C]. There are five operations available.
  The first operation is clock [zero] that expects
  an initial clock. Operation [tick] advances a clock, the parameters
  are an existing [history] and the resulting clock of type [C].
  The [split_l] and [split_r] operations represent how to divide a clock
  into two copies. Two clocks can be merged with [merge].
  *)

  Inductive history :=
  | zero : C -> history
  | tick : history -> C -> history
  | merge : history -> history -> C -> history.

  (** Operation [value] retrieves the embedded clock value. *)

  Definition value c :=
  match c with
  | zero x => x
  | tick _ x => x
  | merge _ _ x => x
  end.

  (** The calculus is paramterized by the 4 abstract operations: zero,
  tick, split, and merge. *)

  Variable Zero: C -> Prop.
  Variable Tick : C -> C -> Prop.
  Variable Merge: C -> C -> C -> Prop.

  Inductive History : history -> Prop :=
  | history_new:
    forall x,
    Zero x ->
    History (zero x)
  | history_tick:
    forall x c,
    Tick (value c) x ->
    History c ->
    History (tick c x)
  | history_merge:
    forall c l r,
    History l ->
    History r ->
    Merge (value l) (value r) c ->
    History (merge l r c).

  Inductive Prev: history -> history -> Prop :=
  | prev_tick:
    forall x c,
    Prev (tick c x) c
  | prev_merge_l:
    forall x c1 c2,
    Prev (merge c1 c2 x) c1
  | prev_merge_r:
    forall x c1 c2,
    Prev (merge c1 c2 x) c2.

  Inductive Edge: (history*history) -> Prop :=
  | edge_tick_hb:
    forall x c,
    Edge (x, tick x c)
  | edge_tick_trans:
    forall x y c,
    Edge (x, y) ->
    Edge (x, tick y c)
  | edge_merge_l:
    forall x c l r,
    Edge (x, l) ->
    Edge (x, merge l r c)
  | edge_merge_r:
    forall x c l r,
    Edge (x, r) ->
    Edge (x, merge l r c).
(*
  Let edge_absurd_zero:
    forall c p,
    ~ Edge (zero c) p.
  Proof.
    unfold not; intros.
    inversion H; subst; clear H.
    - inversion H0; subst; clear H.
  Qed.
*)
  Record timestamp := {
    timestamp_type: history;
    timestamp_spec: History timestamp_type;
  }.

  Definition timestamp_value x := value (timestamp_type x).

  Inductive TEdge: (timestamp * timestamp) % type -> Prop :=
  | t_edge_def:
    forall x y,
    Edge (timestamp_type x, timestamp_type y) ->
    TEdge (x, y).

  Variable hb: C -> C -> Prop.

  (** The interface of a logical clock. *)

  Class Clock := {

    (** The happens-before relation must be a strict-order. *)

    hb_spec: StrictOrder hb;

    (** No event should happen before zero. *)

    hb_zero:
      forall x y,
      Zero y ->
      ~ hb x y;

    hb_zero_to_hb:
      forall x y,
      Zero x ->
      hb x y;

    (** Advancing the clock introducesa happens-before edge. *)

    hb_tick:
      forall x y,
      Tick x y ->
      hb x y;

    (** If [x] reaches [z] and [y] advanced to [z], either [x] is
    [y], or [x] happens before [y]. *)

    hb_tick_inv:
      forall x y z,
      Tick x y ->
      hb z y ->
      z = x \/ hb z x;

    (** When merging [l] and [r] into [x], anything that happens before [l]
     (or [r]) also happens before [x]. *)

    hb_merge:
      forall x l r y,
      Merge l r x ->
      (hb y l \/ hb y r) ->
      hb y x;

    (** When merging [l] and [r] into [x], any [y] that happens before [x]
    either happens before [l] or happens before [x]. *)

    hb_merge_inv:
      forall x y l r,
      Merge l r x ->
      hb y x ->
      hb y l \/ hb y r;
  }.

  Context {LT: Clock}.

  Lemma hb_merge_l:
    forall x l r y,
    Merge l r x ->
    hb y l ->
    hb y x.
  Proof.
    eauto using hb_merge.
  Qed.

  Lemma hb_merge_r:
    forall l r x y,
    Merge l r x ->
    hb y r ->
    hb y x.
  Proof.
    eauto using hb_merge.
  Qed.

  Existing Instance hb_spec.

  Let do_hb (x y:timestamp) :=
  hb (timestamp_value x) (timestamp_value y).

  Let do_hb_Irreflexive:
    Irreflexive do_hb.
  Proof.
    unfold do_hb, Irreflexive, Reflexive.
    intros.
    destruct x; simpl.
    unfold complement.
    simpl.
    apply hb_spec.
  Qed.

  Let do_hb_Transitive:
    Transitive do_hb.
  Proof.
    unfold Transitive, do_hb; intros.
    intros.
    assert (Hx:=hb_spec).
    eauto using StrictOrder_Transitive.
  Qed.

  Global Instance do_hb_StrictOrder : StrictOrder do_hb.
  Proof.
    apply Build_StrictOrder; auto.
  Qed.

  Let do_hb_trans:
    forall x y z,
    do_hb x y ->
    do_hb y z ->
    do_hb x z.
  Proof.
    apply do_hb_Transitive.
  Qed.
(*
  Let history_inv_value:
    forall t x,
    History x t ->
    x = value t.
  Proof.
    intros.
    inversion H; subst; simpl; clear H; auto.
  Qed.
*)

  Let edge_to_hb:
    forall y x,
    History x ->
    History y ->
    Edge (x, y) ->
    hb (value x) (value y).
  Proof.
    induction y; intros;
    inversion H1; subst; clear H1;
    inversion H0; subst; clear H0;
    simpl in *;
    eauto using hb_merge_l, hb_merge_r, hb_tick.
    assert (hb (value x) (value y)) by eauto.
    apply hb_tick in H4.
    apply transitivity with (y0:=value y); auto.
  Qed.

  Let tedge_to_hb:
    forall x y,
    TEdge (x, y) ->
    do_hb x y.
  Proof.
    intros.
    unfold do_hb; inversion H; subst; clear H;
    unfold timestamp_value in *.
    destruct x as (x, Sx), y as (y, Sy); simpl in *.
    eauto.
  Qed.

  Let history_zero:
    forall x z,
    History x ->
    Zero (value z) ->
    Edge (z, x).
  Proof.
    induction x; intros; simpl in *;
    inversion H; subst; clear H.
    - assert (N: ~ hb (value z) c) by eauto using hb_zero.
      contradiction N; eauto using hb_zero_to_hb.
    - eauto using edge_tick_trans, hb_zero_to_hb, hb_tick.
    - eauto using hb_zero_to_hb, edge_merge_l, hb_merge_l.
  Qed.
(*
  Let history_tick_edge:
    forall y x c,
    History x ->
    History y ->
    Tick (value x) c ->
    Edge y (value x, value y) ->
    Edge y (c, value y).
  Proof.
    induction y; intros; simpl in *.
    - inversion H2; subst.
      inversion H3.
    - inversion H2; subst; clear H2.
      + rewrite <- H6 in *.
        apply edge_tick_hb.
      apply edge_cons with (c2:=tick c0.
      eauto using edge_cons, prev_tick.
    - 
  Qed.
*)
  Let hb_trans:
    forall x y z,
    hb x y ->
    hb y z ->
    hb x z.
  Proof.
    assert (Transitive hb). {
      apply (@StrictOrder_Transitive C hb).
      auto using hb_spec.
    }
    auto.
  Qed.

  Let hb_irrefl:
    forall x,
    ~ hb x x.
  Proof.
    assert (Irreflexive hb). {
      apply (@StrictOrder_Irreflexive C hb).
      auto using hb_spec.
    }
    auto.
  Qed.

  Let edge_inv_tick:
    forall y x c,
    Edge (tick x c, y) ->
    Edge (x, y).
  Proof.
    induction y; intros;
    inversion H; subst; clear H;
    try (apply IHy in H1);
    auto using edge_tick_trans, edge_tick_hb.
    - apply IHy1 in H1.
      auto using edge_merge_l.
    - apply IHy2 in H1.
      auto using edge_merge_r.
  Qed.
(*
  Let edge_trans:
    forall y x z,
    Edge (x, y) ->
    Edge (y, z) ->
    Edge (x, z).
  Proof.
    induction y; intros; simpl in *;
    inversion H; subst; clear H; eauto.
    - 
    - inversion H; subst; clear H; eauto.
    - 
  Qed.
*)
  Variable tick_fun:
    forall x y z,
    Tick x y ->
    Tick x z ->
    y = z.


  Let hb_to_edge:
    forall x y,
    History x ->
    History y ->
    hb (value x) (value y) ->
    Edge (x, y).
  Proof.
    induction y; intros;
    inversion H0; subst; clear H0; simpl in *.
    - (*auto using hb_zero.*)
      apply hb_zero in H1; auto; contradiction.
    - apply edge_tick_trans.
      
      assert (Edge (x, y)) by eauto using hb_tick.
      assert (hb (value x) c) by auto using hb_tick.
      apply edge_tick_trans.
      apply edge_tick_trans.
      auto using _trans.
      apply hb_tick_inv in H5; eauto.
      destruct H4.
      + subst.
        auto using edge_tick_hb.
      + assert (Edge ty (x, value ty)) by eauto.
        auto using edge_tick_trans.*)
    - eapply hb_merge_inv in H8; eauto.
      assert (x0 = value ty1) by auto using history_inv_value; subst.
      assert (y0 = value ty2) by auto using history_inv_value; subst.
      destruct H8.
      + apply edge_merge_l.
        eauto.
      + apply edge_merge_r.
        eauto.
  Qed.*)

  Let hb_to_tedge:
    forall x y,
    do_hb x y ->
    TEdge (x, y).
  Proof.
    intros.
    destruct x as (x, Sx), y as (y, Sy); unfold do_hb,timestamp_value in *; simpl in *.
    eapply t_edge_def; simpl; eauto.
    apply t_edge_r; simpl.
    eauto.
  Qed.

  Instance Clock_CausalityRelation : CausalityRelation TEdge do_hb.
  Proof.
    apply Build_CausalityRelation; auto.
  Qed.
End Clock.

Module LeToLt.
  Section Defs.
  Variable A:Type.
  Variable le : A -> A -> Prop.
  Context {LE: PreOrder le}.
  Inductive lt (x y: A) : Prop :=
  | lt_def:
    le x y ->
    ~ le y x ->
    lt x y.

  Lemma lt_to_le:
    forall x y,
    lt x y ->
    le x y.
  Proof.
    intros.
    inversion H.
    assumption.
  Qed.

  Let lt_Irreflexive:
    Irreflexive lt.
  Proof.
    unfold Irreflexive, Reflexive, complement.
    intros.
    inversion H.
    contradiction.
  Qed.

  Let lt_trans:
    forall x y z,
    lt x y ->
    lt y z ->
    lt x z.
  Proof.
    intros.
    apply lt_def. {
      eauto using PreOrder_Transitive, lt_to_le.
    }
    unfold not; intros N.
    destruct H.
    destruct H0.
    assert (le y x) by eauto using PreOrder_Transitive.
    contradiction.
  Qed.

  Let lt_Transitive:
    Transitive lt.
  Proof.
    auto using PreOrder_Transitive.
  Qed.

  Instance lt_StrictOrder : StrictOrder lt.
  Proof.
    auto using Build_StrictOrder.
  Qed.
  End Defs.
End LeToLt.


(* -------------------------------------------------------------------------- *)

Require Import Coq.Arith.Arith.
Require Import Coq.QArith.QArith.

Open Scope nat_scope.


Module FunctionSpace.
Section Defs.

  (**
  A functional interpretation of a clock takes as a basis
  a continuous function that maps rational numbers into non-negative integers.
  *)

  Notation clock := (Q -> nat).

  (** A clock is defined in terms of the less-than-equals operator. *)

  Class Le (f g: clock) : Prop := {
    le_1: forall x, f x <= g x;
  }.

  (** Two clocks are equal when they are point-wise equal, in the Leibniz
  sense. *)

  Class Eq (f g: clock) : Prop := {
    eq_1: forall x, f x = g x;
  }.

  (** A clock [h] is the max of [f] and [g] if it is the
  point-wise maximum. *)

  Class Max (f g h:clock) : Prop := {
    max_1: forall x, h x = Nat.max (f x) (g x);
  }.

  (** Less-than equals is transitive. *)

  Lemma le_trans:
    forall f g h,
    Le f g ->
    Le g h ->
    Le f h.
  Proof.
    intros.
    destruct H, H0.
    eauto using Nat.le_trans, Build_Le.
  Qed.

  (** Equality is trivially transitive as well. *)

  Lemma eq_trans:
    forall f g h,
    Eq f g ->
    Eq g h ->
    Eq f h.
  Proof.
    intros.
    destruct H.
    destruct H0.
    eauto using Build_Eq, Nat.eq_trans with *.
  Qed.

  (** The operands of the max are commutative. *)

  Lemma max_comm:
    forall f g h,
    Max f g h ->
    Max g f h.
  Proof.
    intros.
    inversion H.
    apply Build_Max; intros.
    rewrite Nat.max_comm.
    auto.
  Qed.

  (** From the max we can get the less-than-equals operator (lhs). *)

  Lemma max_to_le_l:
    forall f g h,
    Max f g h ->
    Le f h.
  Proof.
    intros.
    apply Build_Le; intros.
    remember (h x).
    symmetry in Heqn.
    rewrite max_1 in Heqn.
    rewrite <- Heqn.
    auto with *.
  Qed.

  (** From the max we can get the less-than-equals operator (rhs). *)

  Lemma max_to_le_r:
    forall f g h,
    Max f g h ->
    Le g h.
  Proof.
    intros.
    apply max_comm in H.
    eauto using max_to_le_l.
  Qed.

  (** From two less-than-equals we can obtain the equals operator. *)

  Lemma le_to_eq:
    forall f g,
    Le f g ->
    Le g f ->
    Eq f g.
  Proof.
    intros.
    destruct H.
    destruct H0.
    apply Build_Eq; intros.
    auto with *.
  Qed.

  (** And from the equals we can obtain the less-than-equals. *)

  Lemma eq_to_le_1:
    forall f g,
    Eq f g ->
    Le f g.
  Proof.
    intros.
    inversion H; subst; clear H.
    apply Build_Le; intros.
    rewrite eq_2.
    auto with *.
  Qed.

  (** Equality is symmetric. *)

  Lemma eq_symm:
    forall f g,
    Eq f g ->
    Eq g f.
  Proof.
    intros.
    destruct H.
    apply Build_Eq.
    intros.
    symmetry.
    auto.
  Qed.

  (** And from the equals we can obtain the reverse less-than-equals. *)

  Lemma eq_to_le_2:
    forall f g,
    Eq f g ->
    Le g f.
  Proof.
    intros.
    apply eq_symm in H.
    auto using eq_to_le_1.
  Qed.

  (** We define the non-strict less-than as usual, in terms of
  the less-than-equals operator. *)

  Inductive Lt (f g: clock) : Prop :=
  | lt_def:
    Le f g ->
    ~ (Le g f) ->
    Lt f g.

  (** Given a less-then-equals, we only need to find a point that is
  strictly smaller, to conclude that the relation is strictly smaller then.
  *)

  Lemma lt_1:
    forall f g,
    Le f g ->
    forall x,
    f x < g x ->
    Lt f g.
  Proof.
    intros.
    apply lt_def; auto using lt_def.
    unfold not; intros.
    assert (Eq f g) by auto using le_to_eq.
    assert (f x = g x). {
      auto using eq_1.
    }
    subst.
    auto with *.
  Qed.

  (** From the less-than we can obtain a less-than-equals operator. *)

  Lemma lt_to_le:
    forall f g,
    Lt f g ->
    Le f g.
  Proof.
    intros.
    inversion H.
    assumption.
  Qed.

  (** We can use equality to replace operands on the left-hand side of
   the less-than-equals operator. *)

  Lemma le_rw_l:
    forall f g h,
    Le f g ->
    Eq f h ->
    Le h g.
  Proof.
    intros.
    apply Build_Le.
    intros.
    assert (R: h x = f x) by eauto using eq_1.
    rewrite R.
    auto using le_1.
  Qed.

  (** Less-than is transitive. *)

  Lemma lt_trans:
    forall f g h,
    Lt f g ->
    Lt g h ->
    Lt f h.
  Proof.
    intros.
    apply lt_def; eauto using lt_to_le, le_trans.
    unfold not; intros N.
    destruct H.
    destruct H0.
    assert (Le f h) by eauto using le_trans.
    assert (Eq f h) by eauto using le_to_eq.
    assert (Le h g) by eauto using le_rw_l.
    contradiction.
  Qed.

  Inductive Zero : clock -> Prop :=
  | zero_def:
    forall f,
    (forall x, f x = 0) ->
    Zero f.

  Variable Tick : clock -> clock -> Prop.

  Variable Split : clock -> clock -> clock -> Prop.

  Let lt_Irreflexive : Irreflexive Lt.
  Proof.
    unfold Irreflexive, Reflexive, complement.
    intros.
    inversion H.
    contradiction.
  Qed.

  Let lt_Transitive : Transitive Lt.
  Proof.
    unfold Transitive; eauto using lt_trans.
  Qed.

  Let lt_StrictOrder : StrictOrder Lt.
  Proof.
    auto using Build_StrictOrder.
  Qed.

  Class FunClock := {
    hb_tick: forall x y : clock, Tick x y -> Lt x y;
    hb_tick_inv : forall x y z, Tick x y -> Lt z y -> z = x \/ Lt z x;
    hb_split : forall x l r y, Split x l r -> Lt y x -> Lt y l /\ Lt y r;
    hb_split_inv : forall x y l r, Split x l r -> Lt y l \/ Lt y r -> Lt y x;
  }.

  Context {F:FunClock}.

  Let hb_zero:
    forall x y,
    Zero y ->
    ~ Lt x y.
  Proof.
    intros.
    unfold not; intros.
    inversion H.
    subst.
    inversion H0; subst; clear H0.
    contradiction H3.
    apply Build_Le; intros.
    assert (R := H1 x0).
    rewrite R.
    auto with *.
  Qed.

  Let hb_max_l:
    forall x l r y,
    Max l r x ->
    Lt y l ->
    Lt y x.
  Proof.
    intros.
    apply lt_def.
    - apply lt_to_le in H0.
      apply max_to_le_l in H.
      eauto using le_trans.
    - destruct H0.
      unfold not; intros N.
      contradiction H1; clear H1.
      apply max_to_le_l in H.
      eauto using le_trans.
  Qed.

  Let hb_max_r:
    forall x l r y,
    Max l r x ->
    Lt y r ->
    Lt y x.
  Proof.
    intros.
    apply lt_def.
    - apply lt_to_le in H0.
      apply max_to_le_r in H.
      eauto using le_trans.
    - destruct H0.
      unfold not; intros N.
      contradiction H1; clear H1.
      apply max_to_le_r in H.
      eauto using le_trans.
  Qed.

  Let hb_max:
    forall x l r y,
    Max l r x ->
    Lt y l \/ Lt y r ->
    Lt y x.
  Proof.
    intros.
    destruct H0; eauto.
  Qed.

  Variable le_dec:
    forall x y,
    { Le x y } + { ~ Le x y }.

  Let lt_dec:
    forall x y,
    { Lt x y } + { ~ Lt x y}.
  Proof.
    intros.
    destruct (le_dec x y). {
      destruct (le_dec y x). {
        right.
        unfold not; intros.
        destruct H.
        contradiction.
      }
      auto using lt_def.
    }
    right.
    unfold not; intros.
    contradiction n.
    auto using lt_to_le.
  Qed.

  Variable MaxPre: clock -> clock -> Prop.

  Variable hb_max_inv:
    forall x y l r,
    MaxPre l r ->
    Max l r x ->
    Lt y x ->
    Lt y l \/ Lt y r.

  Global Instance fun_historyo_clock :
    Clock Zero Tick Split (fun l r x => Max l r x /\ MaxPre l r) Lt.
  Proof.
    apply Build_Clock; eauto using hb_tick, hb_tick_inv, hb_split, hb_split_inv.
    - intros.
      destruct H.
      eauto.
    - intros.
      destruct H.
      eauto.
  Qed.
(*
  (** Projection takes a (boolean) membership function yielding
  zero whenever the predicate is false, otherwise returning the value
  of the given function. *)

  Definition project (p:Q->bool) (f:clock) x :=
  if p x then f x else 0.

  Lemma projection_le:
    forall x y p,
    Le x y ->
    Le (project p x) y.
  Proof.
    intros.
    apply Build_Le; intros.
    unfold project.
    destruct (p x0); auto with *.
    auto using le_1.
  Qed.

  (** Restriction is defined as the negation (the inverse predicate)
  of projection. *)

  Definition restrict (r:Q->bool) := project (fun x => negb (r x)).

  (** Function [d] denotes (interprets) an element of [C] as a clock.
  Function [p] denotes a projection such that [I] is the index of projection.
  Function [merge] yields the synchronization of two clocks.
  Partial function [advance] advances the logical time of a subset of [C]
  according to a projection [I].
  Property [merge_1] states that merge implements a point-wise
  max between [d x] and [d y].
  Property [advance_1] states that function advance produces a clock that is
  strictly greater than the clock in its argument.
  Property [advance_2] states that function advance should leave the
  rest of the clock intact, with respect to the projection given by [i]. *)

  Class Clock {C I}
  {d: C -> clock}
  (p: I -> Q -> bool)
  (merge: C -> C -> C)
  (advance: I -> C -> option C) := {
    merge_1:
      forall x y,
      Max (d x) (d y) (d (merge x y));
    advance_1:
      forall i x y,
      advance i x = Some y ->
      Lt (d x) (d y);
  }.
  *)
End Defs.
End FunctionSpace.
