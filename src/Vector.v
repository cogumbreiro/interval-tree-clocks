Require Import Coq.Lists.List.
Require Import Coq.FSets.FMaps.
Require Import Coq.FSets.FMapAVL.
Require Import Coq.Arith.Arith.
Require Import Aniceto.Map.
Require Import Coq.omega.Omega.

Set Implicit Arguments.

Module TID := Nat_as_OT.
Module TID_Facts := OrderedTypeFacts TID.
Module Map_TID := FMapAVL.Make TID.
Module Map_TID_Facts := FMapFacts.Facts Map_TID.
Module Map_TID_Props := FMapFacts.Properties Map_TID.
Module Map_TID_Extras := MapUtil Map_TID.

Import ListNotations.

Module Causality.
  Inductive t :=
  | root: t
  | event: t -> t
  | child: t -> t.

  Inductive HappensBefore: t -> t -> Prop :=
  | happens_before_event:
    forall x,
    HappensBefore x (event x)
  | happens_before_child:
    forall x,
    HappensBefore x (child x)
  | happens_before_cons_event:
    forall x y,
    HappensBefore x y ->
    HappensBefore x (event y)
  | happens_before_cons_child:
    forall x y,
    HappensBefore x y ->
    HappensBefore x (child y).

  Lemma happens_before_root_event:
    forall x,
    HappensBefore root (event x).
  Proof.
    induction x; intros.
    - apply happens_before_event.
    - auto using happens_before_cons_event.
    - inversion IHx; subst; clear IHx. {
        auto using happens_before_cons_event, happens_before_child.
      }
      inversion H1; subst; clear H1;
      auto using happens_before_cons_event, happens_before_child,
      happens_before_cons_child, happens_before_event.
  Qed.

  Lemma happens_before_root_child:
    forall x,
    HappensBefore root (child x).
  Proof.
    induction x; intros.
    - apply happens_before_child.
    - inversion IHx; subst; clear IHx. {
        auto using happens_before_cons_child, happens_before_event.
      }
      inversion H1; subst; clear H1;
      auto using happens_before_cons_event, happens_before_child,
      happens_before_cons_child, happens_before_event.
    - auto using happens_before_cons_child.
  Qed.

End Causality.
(*
Module Task.
  Inductive join_tree :=
    | empty
    | merge: (Causality.t * join_tree) -> join_tree -> join_tree.

  Definition t := (Causality.t * join_tree) % type.

  Definition spawn (task:t) :=
  let (l, js) := task in (Causality.event l, js).

  Definition join (task:t) (other:t) : t :=
  let (l, js) := task in (l, merge other js).

End Task.
*)
Module Timestamp.
  Inductive HappensBefore: list Causality.t -> list Causality.t -> Prop :=
  | happens_before_def:
    forall ts1 ts2 x y,
    In x ts1 ->
    In y ts2 ->
    Causality.HappensBefore x y ->
    Forall (fun z => Forall (fun w => ~ Causality.HappensBefore w z) ts2) ts1 ->
    HappensBefore ts1 ts2.
End Timestamp.
(*
Module VectorClock.
Section Defs.

  Definition vectorclock := Map_TID.t nat.

  Definition timestamp := (nat * vectorclock) % type.

  Let union_one (k:nat) (n:nat) (vc:vectorclock) : vectorclock :=
  match Map_TID.find k vc with
  | Some n' => Map_TID.add k (Nat.max n n') vc
  | _ => Map_TID.add k n vc
  end.

  Definition union (l r:vectorclock) : vectorclock :=
  Map_TID.fold union_one l r.

  Inductive Build: Causality.t -> timestamp -> Prop :=
  | build_root:
    Build Causality.root (0, Map_TID.add 0 1 (Map_TID.empty nat))
  | build_event:
    forall e t vc n,
    Build e (t, vc) ->
    Map_TID.MapsTo t n vc -> 
    Build (Causality.event e) (t, Map_TID.add t (S n) vc)
  | build_child:
    forall e t vc,
    Build e (t, vc) ->
    ~ Map_TID.In (S (Causality.size e)) vc ->
    Build (Causality.child e) (S (Causality.size e), Map_TID.add (S (Causality.size e)) 1 vc)
  | build_merge:
    forall l r tl tr vcl vcr,
    Build l (tl, vcl) ->
    Build r (tr, vcr) ->
    Build (Causality.merge l r) (tl, union vcl vcr).

  Definition SubMap (x y:Map_TID.t nat) :=
    forall k v1 v2, Map_TID.MapsTo k v1 x -> Map_TID.MapsTo k v2 y -> v1 <= v2.

  Inductive HappensBefore (x y: timestamp) : Prop :=
  | happens_before_lt:
    forall t nx ny,
    Map_TID.MapsTo t nx (snd x) ->
    Map_TID.MapsTo t ny (snd y) ->
    nx < ny ->
    HappensBefore x y
  | happens_before_sub:
    forall t,
    SubMap (snd x) (snd y) ->
    Map_TID.In t (snd y) ->
    ~ Map_TID.In t (snd x) ->
    HappensBefore x y.

  Lemma build_fun:
    forall c x y,
    Build c x ->
    Build c y ->
    x = y.
  Proof.
    induction c; intros;
    inversion H; inversion H0; subst; auto.
    - assert (Heq: (t0,vc0) = (t,vc)) by auto.
      inversion Heq; subst; clear Heq.
      assert (n0 = n) by eauto using Map_TID_Facts.MapsTo_fun; subst.
      trivial.
    - assert (Heq: (t0,vc0) = (t,vc)) by auto;
      inversion Heq; subst; clear Heq; auto.
    - assert (Heq: (tl0,vcl0) = (tl,vcl)) by auto;
      inversion Heq; subst; clear Heq.
      assert (Heq: (tr0,vcr0) = (tr,vcr)) by auto;
      inversion Heq; subst; clear Heq.
      trivial.
  Qed.

  Let inc_hb:
    forall t n vc,
    Map_TID.MapsTo t n vc ->
    HappensBefore (t, vc) (t, Map_TID.add t (S n) vc).
  Proof.
    intros.
    eapply happens_before_lt; eauto; simpl; auto using Map_TID.add_1.
  Qed.

  Let sub_map_add_3:
    forall x n vc,
    ~ Map_TID.In x vc ->
    SubMap vc (Map_TID.add x n vc).
  Proof.
    unfold SubMap; intros.
    destruct (TID.eq_dec k x). {
      subst.
      contradiction H.
      eauto using Map_TID_Extras.mapsto_to_in.
    }
    apply Map_TID.add_3 in H1; auto.
    assert (v1 = v2) by eauto using Map_TID_Facts.MapsTo_fun; subst; auto.
  Qed.

  Let sub_map_add_1:
    forall x y t n,
    Map_TID.MapsTo t n y ->
    SubMap x y ->
    SubMap x (Map_TID.add t (S n) y).
  Proof.
    unfold SubMap; intros.
    eapply Map_TID_Facts.add_mapsto_iff in H2.
    destruct H2 as [(?,?)|(?,?)]; subst. {
      assert (v1 <= n). {
        eapply H0; eauto.
      }
      auto.
    }
    eapply H0; eauto.
  Qed.

  Let create_hb:
    forall x y vc,
    ~ Map_TID.In y vc ->
    HappensBefore (x, vc) (y, Map_TID.add y 1 vc).
  Proof.
    intros.
    apply happens_before_sub with (t:=y); simpl; auto.
    rewrite Map_TID_Facts.add_in_iff.
    auto.
  Qed.

  Let contains_to_hb:
    forall x y cx cy,
    Build cx x ->
    Build cy y ->
    Causality.Contains cx cy ->
    HappensBefore x y.
  Proof.
    intros.
    inversion H1; subst; clear H1;
    inversion H0; subst; clear H0.
    - assert (x = (t,vc)) by eauto using build_fun; subst.
      eapply happens_before_lt; simpl; eauto using Map_TID.add_1 with *.
    - assert (x = (t,vc)) by eauto using build_fun; subst.
      eapply happens_before_sub; simpl; eauto with *.
      rewrite Map_TID_Facts.add_in_iff.
      auto.
  Qed.

  Let hb_add:
    forall t vc x n a b,
    Map_TID.MapsTo t n vc ->
    HappensBefore x (a, vc) ->
    HappensBefore x (b, Map_TID.add t (S n) vc).
  Proof.
    intros.
    inversion H0; subst; clear H0;
    simpl in *.
    - destruct (TID.eq_dec t0 t). {
        subst.
        assert (ny = n) by eauto using Map_TID_Facts.MapsTo_fun; subst.
        apply happens_before_lt with (t:=t) (nx:=nx) (ny:=S n); simpl; auto using Map_TID.add_1 with *.
      }
      eapply happens_before_lt; eauto using Map_TID.add_2; simpl in *.
    - apply happens_before_sub with (t:=t0); simpl; auto.
      rewrite Map_TID_Facts.add_in_iff.
      auto.
  Qed.
(*
  Lemma happens_before_spec_1:
    forall cy cx x y,
    Build cx x ->
    Build cy y ->
    Causality.HappensBefore cx cy ->
    HappensBefore x y.
  Proof.
    induction cy; intros.
    - inversion H1; subst; clear H1.
      inversion H2; subst; clear H2.
    - inversion H0; subst; clear H0.
      inversion H1; subst; clear H1.
      + inversion H0; subst; clear H0.
        assert (x = (t, vc)) by eauto using build_fun; subst.
        auto.
      + assert (HB: HappensBefore x (t, vc)) by eauto.
        eauto.
    - inversion H0; subst; clear H0.
      inversion H1; subst; clear H1.
      + inversion H0; subst; clear H0.
        assert (x = (t, vc)) by eauto using build_fun; subst.
        auto.
      + assert (HB: HappensBefore x (t, vc)) by eauto.
        
  Qed.
  *)
End Defs.
End VectorClock.
*)

Module TreeId.

  Definition t := list nat.

  Inductive HappensBefore: t -> t -> Prop :=
  | happens_before_event:
    forall n m x,
    n < m ->
    HappensBefore (n::x) (m::x)
  | happens_before_child:
    forall n x m,
    HappensBefore (n::x) (m::n::x)
  | happens_before_cons:
    forall x y n,
    HappensBefore x y ->
    HappensBefore x (n::y).

  Inductive HappensBefore2: t -> t -> Prop :=
  | happens_before2_event:
    forall n m x,
    n < m ->
    HappensBefore2 (n::x) (m::x)
  | happens_before2_child:
    forall n x,
    HappensBefore2 x (S n::x)
  | happens_before2_cons:
    forall x y n,
    HappensBefore2 x y ->
    HappensBefore2 x (n::y).

  Let happens_before_app:
    forall z x y,
    HappensBefore x y ->
    HappensBefore x (z ++ y).
  Proof.
    induction z; intros; auto.
    apply IHz in H.
    eauto using happens_before_cons.
  Qed.

  Let happens_before2_app:
    forall z x y,
    HappensBefore2 x y ->
    HappensBefore2 x (z ++ y).
  Proof.
    induction z; intros; auto.
    apply IHz in H.
    eauto using happens_before2_cons.
  Qed.

  Let happens_before_inv_cons:
    forall x y,
    HappensBefore x y ->
    exists n1 n2 l1 l2, x = (n1::l1) /\ y = (n2::l2).
  Proof.
    intros.
    induction H;
    try (destruct IHHappensBefore as (n1, (n2, (l1, (l2, (?,?)))));
      subst);
    eauto 6.
  Qed.
(*
  Let happens_before2_inv_cons:
    forall x y,
    HappensBefore2 x y ->
    exists n1 n2 l1 l2, x = (n1::l1) /\ y = (n2::l2).
  Proof.
    intros.
    induction H;
    try (destruct IHHappensBefore2 as (n1, (n2, (l1, (l2, (?,?)))));
      subst);
    eauto 6.
  Qed.
*)
  Let happens_before_inv_cons_l:
    forall x y,
    HappensBefore x y ->
    exists n l, x = n::l.
  Proof.
    intros.
    apply happens_before_inv_cons in H.
    destruct H as (?, (?, (?, (?, (?, ?))))).
    subst.
    eauto.
  Qed.
(*
  Let happens_before2_inv_cons_l:
    forall x y,
    HappensBefore2 x y ->
    exists n l, x = n::l.
  Proof.
    intros.
    apply happens_before2_inv_cons in H.
    destruct H as (?, (?, (?, (?, (?, ?))))).
    subst.
    eauto.
  Qed.
*)
  Let happens_before_absurd_nil:
    forall x,
    ~ HappensBefore [] x.
  Proof.
    induction x; unfold not; intros N;
    inversion N; subst; clear N.
    contradiction.
  Qed.
(*
  Let happens_before2_absurd_nil:
    forall x,
    ~ HappensBefore2 [] x.
  Proof.
    induction x; unfold not; intros N;
    inversion N; subst; clear N.
    contradiction.
  Qed.
*)
  Let happens_before_not_root:
    forall x,
     ~ HappensBefore x [0].
  Proof.
    unfold not; intros.
    inversion H; subst; clear H. {
      omega.
    }
    inversion H2.
  Qed.

  Let happens_before2_not_root:
    forall x,
     ~ HappensBefore2 x [].
  Proof.
    unfold not; intros.
    inversion H; subst; clear H.
  Qed.

  Inductive Build: Causality.t -> t -> Prop :=
  | build_root:
    Build Causality.root [0]
  | build_event:
    forall e n l,
    Build e (n::l) ->
    Build (Causality.event e) (S n :: l)
  | build_child:
    forall e n l,
    Build e (n::l) ->
    Build (Causality.child e) (0::n::l).

  Inductive Build2: Causality.t -> t -> Prop :=
  | build2_root:
    Build2 Causality.root []
  | build2_event:
    forall e n l,
    Build2 e (n::l) ->
    Build2 (Causality.event e) (S n :: l)
  | build2_child:
    forall e l,
    Build2 e l ->
    Build2 (Causality.child e) (1::l).

  Let build_fun_l:
    forall x y l,
    Build x l ->
    Build y l ->
    x = y.
  Proof.
    induction x; intros;
    inversion H; subst; clear H;
    inversion H0; subst; clear H0.
    - trivial.
    - assert (x = e) by eauto.
      subst.
      trivial.
    - assert (x = e) by eauto.
      subst.
      trivial.
  Qed.

  Let build2_fun_l:
    forall x y l,
    Build2 x l ->
    Build2 y l ->
    x = y.
  Proof.
    induction x; intros;
    inversion H; subst; clear H;
    inversion H0; subst; clear H0.
    - trivial.
    - assert (x = e) by eauto.
      subst.
      trivial.
    - inversion H2.
    - inversion H3.
    - assert (x = e) by eauto.
      subst.
      trivial.
  Qed.

  Let build_fun_r:
    forall x l1 l2,
    Build x l1 ->
    Build x l2 ->
    l1 = l2.
  Proof.
    induction x; intros;
    inversion H; subst; clear H;
    inversion H0; subst; clear H0.
    - trivial.
    - assert (n :: l = n0 :: l0) by eauto.
      inversion H; subst.
      trivial.
    - assert (n :: l = n0 :: l0) by eauto.
      inversion H; subst.
      trivial.
  Qed.

  Let build2_fun_r:
    forall x l1 l2,
    Build2 x l1 ->
    Build2 x l2 ->
    l1 = l2.
  Proof.
    induction x; intros;
    inversion H; subst; clear H;
    inversion H0; subst; clear H0.
    - trivial.
    - assert (n :: l = n0 :: l0) by eauto.
      inversion H; subst.
      trivial.
    - assert (l = l0) by eauto.
      subst.
      trivial.
  Qed.

  Let cons_absurd:
    forall {A} l (a:A),
    ~ (a :: l = l).
  Proof.
    induction l; unfold not; intros;
    inversion H; subst; clear H.
    apply IHl in H2.
    assumption.
  Qed.

  Let happens_before_length:
    forall y x,
    HappensBefore x y ->
    length x <= length y.
  Proof.
    induction y; intros;
    inversion H; subst; clear H; simpl; auto.
  Qed.

  Let happens_before2_length:
    forall y x,
    HappensBefore2 x y ->
    length x <= length y.
  Proof.
    induction y; intros;
    inversion H; subst; clear H; simpl; auto.
  Qed.

  Let happens_before_absurd_cons_l:
    forall l n,
    ~ HappensBefore (n :: l) l.
  Proof.
    unfold not; intros.
    apply happens_before_length in H.
    simpl in *.
    omega.
  Qed.

  Let happens_before2_absurd_cons_l:
    forall l n,
    ~ HappensBefore2 (n :: l) l.
  Proof.
    unfold not; intros.
    apply happens_before2_length in H.
    simpl in *.
    omega.
  Qed.

  Let nil_neq_app_cons:
    forall {A} l1 (m:A) l2,
    [] <> l1 ++ m :: l2.
  Proof.
    induction l1;
    unfold not;
    simpl; 
    intros;
    inversion H.
  Qed.

  Let build_inv_cons:
    forall c n m l,
    Build c (n :: m :: l) ->
    exists c', Build c' (m::l).
  Proof.
    induction c; intros;
    inversion H; subst; clear H;
    eauto using build_event.
  Qed.

  Let build2_inv_cons:
    forall c n m l,
    Build2 c (n :: m :: l) ->
    exists c', Build2 c' (m::l).
  Proof.
    induction c; intros;
    inversion H; subst; clear H;
    eauto using build2_event.
  Qed.

  Let build_inv_app:
    forall c l1 m l2,
    Build c (l1 ++ m :: l2) ->
    exists c', Build c' (m :: l2).
  Proof.
    induction c; intros.
    - destruct l1;
      simpl in *;
      inversion H; subst; clear H.
      + eauto using build_root.
      + apply nil_neq_app_cons in H2; contradiction.
    - destruct l1;
      simpl in *;
      inversion H; subst; clear H. {
        eauto using build_event.
      }
      assert (R: n0 :: l1 ++ m :: l2 = (n0 :: l1) ++ m :: l2) by auto with *.
      rewrite R in *; clear R.
      apply IHc in H2.
      assumption.
    - destruct l1; simpl in *;
      inversion H; subst; clear H. {
        eauto using build_child.
      }
      destruct l1; simpl in *;
      inversion H3; subst; clear H3. {
        eauto.
      }
      assert (R: n :: l1 ++ m :: l2 = (n :: l1) ++ m :: l2) by auto with *.
      rewrite R in *; clear R.
      apply IHc in H2.
      assumption.
  Qed.

  Let build2_inv_app:
    forall c l1 m l2,
    Build2 c (l1 ++ m :: l2) ->
    exists c', Build2 c' (m :: l2).
  Proof.
    induction c; intros.
    - destruct l1;
      simpl in *;
      inversion H; subst; clear H.
      (*
      + eauto using build2_root.
      + apply nil_neq_app_cons in H2; contradiction.*)
    - destruct l1;
      simpl in *;
      inversion H; subst; clear H. {
        eauto using build2_event.
      }
      assert (R: n0 :: l1 ++ m :: l2 = (n0 :: l1) ++ m :: l2) by auto with *.
      rewrite R in *; clear R.
      eauto.
    - destruct l1; simpl in *;
      inversion H; subst; clear H. {
        eauto using build2_child.
      }
      destruct l1; simpl in *. {
        eauto.
      }
      assert (R: n :: l1 ++ m :: l2 = (n :: l1) ++ m :: l2) by auto with *.
      rewrite R in *; clear R.
      eauto.
  Qed.

  Let happens_before_inv_lt:
    forall l n m,
    HappensBefore (n :: l) (m :: l) ->
    n < m.
  Proof.
    induction l; intros. {
      inversion H; subst; auto; inversion H2.
    }
    inversion H; subst; clear H.
    - assumption.
    - apply cons_absurd in H5; contradiction.
    - apply happens_before_length in H2.
      simpl in *.
      omega.
  Qed.

  Let happens_before2_inv_lt:
    forall l n m,
    HappensBefore2 (n :: l) (m :: l) ->
    n < m.
  Proof.
    induction l; intros. {
      inversion H; subst; auto; inversion H2.
    }
    inversion H; subst; clear H.
    - assumption.
    - apply cons_absurd in H4; contradiction.
    - apply happens_before2_length in H2.
      simpl in *.
      omega.
  Qed.

  Let happens_before_inv_zero:
    forall y x,
    HappensBefore x (0 :: y) ->
    x = y \/ HappensBefore x y.
  Proof.
    induction y; intros. {
      inversion H; subst; clear H. {
        omega.
      }
      inversion H2.
    }
    inversion H; subst; clear H;
    auto; omega.
  Qed.

  Let happens_before2_inv_zero:
    forall y x,
    HappensBefore2 x (0 :: y) ->
    x = y \/ HappensBefore2 x y.
  Proof.
    induction y; intros. {
      inversion H; subst; clear H. {
        omega.
      }
      inversion H2.
    }
    inversion H; subst; clear H;
    auto; omega.
  Qed.

  Fixpoint trailing_zeros l :=
  match l with 
  | 0 :: l => trailing_zeros l
  | _ => l
  end.

  Let happens_before2_inv_one:
    forall y x,
    HappensBefore2 x (1 :: y) ->
    trailing_zeros x = [] \/ trailing_zeros x = trailing_zeros y \/ HappensBefore2 x y.
  Proof.
    induction y; intros. {
      inversion H; subst; clear H.
      - destruct n; auto with *; omega.
      - auto.
      - inversion H2.
    }
    inversion H; subst; clear H; auto.
    destruct n. {
      simpl in *.
      auto.
    }
    simpl in *.
    omega.
  Qed.

  Let happens_before_inv_succ:
    forall x y n,
    HappensBefore x (S n :: y) ->
    x = y \/ (exists m, x = m :: y /\ m < S n) \/ HappensBefore x y.
  Proof.
    induction x; intros. {
      apply happens_before_absurd_nil in H.
      contradiction.
    }
    inversion H; subst; clear H; eauto.
  Qed.

  Let happens_before2_inv_succ:
    forall x y n,
    HappensBefore2 x (S n :: y) ->
    x <> nil ->
    x = y \/ (exists m, x = m :: y /\ m < S n) \/ HappensBefore2 x y.
  Proof.
    induction x; intros. {
      contradiction.
    }
    inversion H; subst; clear H; eauto.
  Qed.

  Let build_inv_l:
    forall c l,
    Build c l ->
    exists n l', l = n::l'.
  Proof.
    intros.
    inversion H; subst; clear H; eauto.
  Qed.
(*
  Let build2_inv_l:
    forall c l,
    Build2 c l ->
    exists n l', l = n::l'.
  Proof.
    intros.
    inversion H; subst; clear H; eauto.
  Qed.
*)
  Lemma happens_before2_spec_2:
    forall cy cx x y,
    Build2 cx x ->
    Build2 cy y ->
    HappensBefore2 x y ->
    Causality.HappensBefore cx cy.
  Proof.
    induction cy; intros;
    inversion H0; subst; clear H0.
    - apply happens_before2_not_root in H1; contradiction.
    - assert (R: x = nil \/ x <> nil). {
        destruct x; auto.
        right.
        unfold not; intros N; inversion N.
      }
      destruct R as [?|R]. {
        subst.
        inversion H; subst.
        auto using Causality.happens_before_root_event.
      }
      apply happens_before2_inv_succ in H1; auto.
      destruct H1 as [?|[(m, (?, ?))|Hb]].
      * subst.
        assert (Hx := H).
        eapply Causality.happens_before_cons_event.
        destruct n. { inversion H3. }
        eauto using happens_before2_child, Causality.happens_before_cons_event.
      * subst.
        assert (Ho: m < n \/ m = n) by omega; clear H1.
        destruct Ho.
        + assert (Hx:HappensBefore2 (m::l) (n::l)) by
          auto using happens_before2_event.
          eapply IHcy in Hx; eauto using Causality.happens_before_cons_event.
        + subst.
          assert (cx = cy) by eauto using build2_fun_l; subst.
          auto using Causality.happens_before_event.
      * eauto using happens_before2_cons, Causality.happens_before_cons_event.
    - apply happens_before2_inv_one in H1.
      destruct x. {
        simpl in *.
        inversion H.
        eauto using Causality.happens_before_root_child.
      }
      destruct n; simpl in *. {
        inversion H.
      }
      destruct H1 as [N|[R|?]].
      + inversion N.
      + destruct l. {
          simpl in *.
          inversion R.
        }
        destruct n0. {
          simpl in *.
          inversion H3.
        }
        simpl in *.
        inversion R; subst; clear R.
        assert (cx = cy) by eauto using build2_fun_l; subst.
        auto using Causality.happens_before_child.
      + eapply IHcy in H0; eauto using Causality.happens_before_cons_child.
  Qed.

  Let happens_before_event_r:
    forall y x n m,
    HappensBefore x (n :: y) ->
    n < m ->
    HappensBefore x (m :: y).
  Proof.
    induction y; intros. {
      inversion H; subst; clear H. {
        auto using happens_before_event with *.
      }
      inversion H3.
    }
    inversion H; subst; clear H.
    - auto using happens_before_event with *.
    - auto using happens_before_child.
    - auto using happens_before_cons.
  Qed.

  Let happens_before2_event_r:
    forall y x n m,
    HappensBefore2 x (n :: y) ->
    n < m ->
    HappensBefore2 x (m :: y).
  Proof.
    induction y; intros. {
      inversion H; subst; clear H.
      - auto using happens_before2_event with *.
      - destruct m. {
          omega.
        }
        auto using happens_before2_child.
      - inversion H3.
    }
    inversion H; subst; clear H.
    - auto using happens_before2_event with *.
    - destruct m. { omega. }
      auto using happens_before2_child.
    - auto using happens_before2_cons.
  Qed.

  Let happens_before_succ:
    forall y x n,
    HappensBefore x (n :: y) ->
    HappensBefore x (S n :: y).
  Proof.
    eauto using happens_before_event_r with *.
  Qed.

  Let happens_before2_succ:
    forall y x n,
    HappensBefore2 x (n :: y) ->
    HappensBefore2 x (S n :: y).
  Proof.
    eauto using happens_before2_event_r with *.
  Qed.

  Lemma happens_before_spec_1:
    forall cy cx x y,
    Build cx x ->
    Build cy y ->
    Causality.HappensBefore cx cy ->
    HappensBefore x y.
  Proof.
    induction cy; intros;
    inversion H1; subst; clear H1;
    inversion H0; subst; clear H0.
    - assert (x = n::l) by eauto using build_fun_r; subst;
      auto using happens_before_event.
    - assert (Hb: HappensBefore x (n :: l)) by eauto.
      auto using happens_before_succ.
    - assert (x = n::l) by eauto using build_fun_r; subst.
      auto using happens_before_child.
    - assert (HappensBefore x (n::l)) by eauto.
      auto using happens_before_cons.
  Qed.

  Lemma happens_before2_spec_1:
    forall cy cx x y,
    Build2 cx x ->
    Build2 cy y ->
    Causality.HappensBefore cx cy ->
    HappensBefore2 x y.
  Proof.
    induction cy; intros;
    inversion H1; subst; clear H1;
    inversion H0; subst; clear H0.
    - assert (x = n::l) by eauto using build2_fun_r; subst;
      auto using happens_before2_event.
    - assert (Hb: HappensBefore2 x (n :: l)) by eauto.
      auto using happens_before2_succ.
    - assert (x = l) by eauto using build2_fun_r; subst.
      auto using happens_before2_child.
    - assert (HappensBefore2 x l) by eauto.
      auto using happens_before2_cons.
  Qed.

  Lemma happens_before_spec:
    forall cy cx x y,
    Build cx x ->
    Build cy y ->
    HappensBefore x y <->
    Causality.HappensBefore cx cy.
  Proof.
    split; eauto using happens_before_spec_1, happens_before_spec_2.
  Qed.

  Lemma happens_before2_spec:
    forall cy cx x y,
    Build2 cx x ->
    Build2 cy y ->
    HappensBefore2 x y <->
    Causality.HappensBefore2 cx cy.
  Proof.
    split; eauto using happens_before2_spec_1, happens_before2_spec_2.
  Qed.
End TreeId.

Module JoinTree.

  Inductive t :=
  | empty : t
  | leaf : TreeId.t -> t
  | node : t -> t -> t.

  Inductive In: TreeId.t -> t -> Prop :=
  | in_leaf:
    forall x,
    In x (leaf x)
  | in_node_l:
    forall x l r,
    In x l ->
    In x (node l r)
  | in_node_r:
    forall x l r,
    In x r ->
    In x (node l r).

End JoinTree.

Section Defs.

  Inductive timestamp := Timestamp: nat -> TreeId.t -> JoinTree.t -> timestamp.

  Definition root := Timestamp 0 [] JoinTree.empty.

  Definition child (ts:timestamp) : timestamp :=
  match ts with
  | Timestamp n t jt => Timestamp 0 (S n::t) jt
  end.

  Definition event (ts:timestamp) : timestamp :=
  match ts with
  | Timestamp n t jt => Timestamp (S n) t jt
  end.

  Definition as_node (ts:timestamp) : JoinTree.t :=
  match ts with
  | Timestamp n t jt => JoinTree.node (JoinTree.leaf (n::t)) jt
  end.

  Definition union (l:timestamp) (r:timestamp) :=
  match l with
  | Timestamp n t jt  => Timestamp n t (JoinTree.node jt (as_node r))
  end.

  Inductive In: TreeId.t -> timestamp -> Prop :=
  | in_eq:
    forall n x j,
    In (n :: x) (Timestamp n x j)
  | in_tree:
    forall n x j y,
    JoinTree.In x j ->
    In x (Timestamp n y j).

  Inductive HappensBefore: timestamp -> timestamp -> Prop :=
  | happens_before_def:
    forall x y l r,
    In x l ->
    In y r ->
    TreeId.HappensBefore x y ->
    HappensBefore l r.

  Inductive Build: Causality.t -> timestamp -> Prop :=
  | build_root:
    Build Causality.root (Timestamp 0 [] JoinTree.empty)
  | build_event:
    forall e x n j,
    Build e (Timestamp n x j) ->
    Build (Causality.event e) (Timestamp (S n) x j)
  | build_child:
    forall e n x j,
    Build e (Timestamp n x j) ->
    Build (Causality.child e) (Timestamp 0 (S n::x) j)
  | build_merge:
    forall cl cr n t jt r,
    Build cl (Timestamp n t jt) ->
    Build cr r ->
    Build (Causality.merge cl cr) (Timestamp n t (JoinTree.node jt (as_node r))).

  Lemma build_fun:
    forall c x y,
    Build c x ->
    Build c y ->
    x = y.
  Proof.
    induction c; intros;
    inversion H; inversion H0; subst; auto; clear H H0.
    - assert (Heq: Timestamp n0 x1 j0 = Timestamp n x0 j) by auto.
      inversion Heq; subst; clear Heq.
      trivial.
    - assert (Heq: Timestamp n0 x1 j0 = Timestamp n x0 j) by auto;
      inversion Heq; subst; clear Heq.
      trivial.
    - assert (Heq: Timestamp n0 t0 jt0 = Timestamp n t jt) by auto;
      inversion Heq; subst; clear Heq.
      assert (r0 = r) by auto; subst.
      trivial.
  Qed.

  Let happens_before_inc:
    forall n x j,
    HappensBefore (Timestamp n x j) (Timestamp (S n) x j).
  Proof.
    intros.
    eapply happens_before_def; eauto using in_eq, TreeId.happens_before_lt.
  Qed.

  Let in_inv_empty:
    forall x n t,
    In x (Timestamp n t JoinTree.empty) ->
    x = n::t.
  Proof.
    intros.
    inversion H; subst; clear H; auto.
    inversion H2; subst; clear H2.
  Qed.
(*
  Let happens_before_trans:
    forall y x z,
    HappensBefore x y ->
    HappensBefore y z ->
    HappensBefore x z.
  Proof.
    intros.
    inversion H; subst; clear H.
    inversion H0; subst; clear H0.
    eapply happens_before_def; eauto.
    eauto using TreeId.happens_before_trans.
    eauto using TreeId.happens_before_trans, happens_before_def.
    induction j; intros;
    inversion H; subst; clear H;
    inversion H0; subst; clear H0.
    - apply in_inv_empty in H2.
      apply in_inv_empty in H.
      subst.
      inversion H2; subst; clear H2.
      + 
  Qed.
*)

  Let happens_before_inc_rhs:
    forall x y n ts j,
    In x ts ->
    TreeId.HappensBefore x (n :: y) ->
    HappensBefore ts (Timestamp (S n) y j).
  Proof.
    eauto using happens_before_def, in_eq, TreeId.happens_before_inc_rhs.
  Qed.

  Let happens_before_event_lhs:
    forall ts n x j,
    HappensBefore ts (Timestamp n x j) ->
    HappensBefore ts (Timestamp (S n) x j).
  Proof.
    intros; inversion H; subst.
    eauto.
    inversion H1; subst; clear H1. {
      eauto.
    }
    eauto using happens_before_def, in_tree.
  Qed.

  Let happens_before_child_rhs:
    forall n x j,
    HappensBefore (Timestamp n x j) (Timestamp 0 ((S n) :: x) j).
  Proof.
    intros.
    apply happens_before_def with (x:=n::x) (y:=0::S n::x); auto using in_eq.
    eauto using TreeId.happens_before_cons, TreeId.happens_before_lt.
  Qed.

  Let happens_before_child_lhs:
    forall ts n x j,
    HappensBefore ts (Timestamp n x j) ->
    HappensBefore ts (Timestamp 0 (S n :: x) j).
  Proof.
    intros.
    inversion H; subst; clear H.
    inversion H1; subst; clear H1. {
      rename x into y.
      rename x0 into x.
      apply happens_before_def with (x:=x) (y:=0::S n::y); auto using in_eq.
      eauto using TreeId.happens_before_cons, TreeId.happens_before_inc_rhs.
    }
    eauto using happens_before_def, in_tree.
  Qed.

  Let in_node_l:
    forall x n t j j',
    JoinTree.In x j ->
    In x (Timestamp n t (JoinTree.node j j')).
  Proof.
    auto using JoinTree.in_node_l, in_tree.
  Qed.

  Let happens_before_node_l:
    forall ts n t j j',
    HappensBefore ts (Timestamp n t j) ->
    HappensBefore ts (Timestamp n t (JoinTree.node j j')).
  Proof.
    intros.
    inversion H; subst; clear H.
    inversion H1; subst; clear H1. {
      eauto using happens_before_def, in_eq.
    }
    eauto using happens_before_def.
  Qed.

  Let in_to_join_tree_in:
    forall x ts,
    In x ts ->
    JoinTree.In x (as_node ts).
  Proof.
    intros.
    destruct ts.
    simpl.
    inversion H; subst; clear H. {
      auto using JoinTree.in_node_l, JoinTree.in_leaf.
    }
    auto using JoinTree.in_node_r.
  Qed.

  Let in_node_r:
    forall x r n t jt,
    In x r ->
    In x (Timestamp n t (JoinTree.node jt (as_node r))).
  Proof.
    auto using in_tree, JoinTree.in_node_r.
  Qed.

  Let happens_before_r:
    forall x r n t jt,
    HappensBefore x r ->
    HappensBefore x (Timestamp n t (JoinTree.node jt (as_node r))).
  Proof.
    intros.
    inversion H; subst; clear H.
    eauto using happens_before_def.
  Qed.

  Lemma happens_before_spec_1:
    forall cy cx x y,
    Build cx x ->
    Build cy y ->
    Causality.HappensBefore cx cy ->
    HappensBefore x y.
  Proof.
    induction cy; intros.
    - inversion H1; subst; clear H1.
      inversion H2; subst; clear H2.
    - inversion H0; subst; clear H0.
      inversion H1; subst; clear H1.
      + inversion H0; subst; clear H0.
        assert (x = Timestamp n x0 j) by eauto using build_fun; subst.
        auto.
      + assert (HB: HappensBefore x (Timestamp n x0 j)) by eauto.
        eauto.
    - inversion H0; subst; clear H0.
      inversion H1; subst; clear H1.
      + inversion H0; subst; clear H0.
        assert (x = Timestamp n x0 j) by eauto using build_fun; subst.
        auto.
      + assert (HB: HappensBefore x (Timestamp n x0 j)) by eauto.
        auto.
    - inversion H0; subst; clear H0.
      inversion H1; subst; clear H1.
      + inversion H0; subst; clear H0.
      + assert (HappensBefore x (Timestamp n t jt)) by eauto.
        auto.
      + assert (HappensBefore x r) by eauto.
        auto.
  Qed.

  Let in_inv_root:
    forall x,
    In x root ->
    x = [0].
  Proof.
    unfold root; intros.
    apply in_inv_empty in H.
    assumption.
  Qed.

  Let hb_not_root:
    forall x,
    ~ HappensBefore x root.
  Proof.
    unfold not; intros ? N.
    inversion N; subst; clear N.
    apply in_inv_root in H0; subst.
    apply TreeId.happens_before_not_root in H1.
    assumption.
  Qed.
(*
  Let hb_inv_event:
    forall cy cx nx lx jx ny ly jy,
    Build cx (Timestamp nx lx jx) ->
    Build cy (Timestamp ny ly jy) ->
    HappensBefore (Timestamp nx lx jx) (Timestamp (S ny) ly jy) ->
    HappensBefore (Timestamp nx lx jx) (Timestamp ny ly jy) \/ cx = cy.
  Proof.
    induction cy; intros.
    - inversion H0; subst; clear H0.
      inversion H1; subst; clear H1.
      assert (y = [1]) by auto; subst.
      assert (x = [0]) by auto using TreeId.happens_before_inv_one; subst.
      
    inversion H1; subst; clear H1.
    inversion H3; subst; clear H3. {
    }
  Qed.
*)
  Lemma happens_before_spec_2:
    forall cy cx x y,
    Build cx x ->
    Build cy y ->
    HappensBefore x y ->
    Causality.HappensBefore cx cy.
  Proof.
    induction cy; intros.
    - inversion H0; subst; clear H0.
      apply hb_not_root in H1; contradiction.
    - inversion H0; subst; clear H0.
      rename x0 into l.
      inversion H1; subst; clear H1.
      rename x1 into tx.
      rename x0 into l.
      rename y into ty.
      inversion H2; subst; clear H2. {
        inversion H4; subst; clear H4. {
          inversion H5.
        }
        apply Causality.happens_before_eq.
        eapply IHcy; eauto.
      }
  Qed.
End Defs.

