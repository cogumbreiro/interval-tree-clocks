Require Import Coq.Arith.Arith.
Require Import Coq.omega.Omega.
Require Import Coq.QArith.QArith.
Require Import One.
Require Import IntervalTreeMap.
Require Import Interpret.

Section Defs.

  Inductive Join : event -> event -> event -> Prop :=
  | join_leaf_leaf:
    forall n1 n2,
    Join (Leaf n1) (Leaf n2) (Leaf (Nat.max n1 n2))
  | join_leaf_node:
    forall n1 n2 l2 r2 e,
    Join (Node n1 (Leaf 0) (Leaf 0)) (Node n2 l2 r2) e ->
    Join (Leaf n1) (Node n2 l2 r2) e
  | join_node_leaf:
    forall n1 l1 r1 n2 e,
    Join (Node n1 l1 r1) (Node n2 (Leaf 0) (Leaf 0)) e ->
    Join (Node n1 l1 r1) (Leaf n2) e
  | join_node_gt:
    forall n1 l1 r1 n2 l2 r2 e,
    n1 > n2 ->
    Join (Node n2 l2 r2) (Node n1 l1 r1) e ->
    Join (Node n1 l1 r1) (Node n2 l2 r2) e
  | join_node_le:
    forall n1 l1 r1 n2 l2 r2 e1 e2,
    n1 <= n2 ->
    Join l1 (lift l2 (n2 - n1)) e1 ->
    Join r1 (lift r2 (n2 - n1)) e2 ->
    Join (Node n1 l1 r1) (Node n2 l2 r2) (Node n1 e1 e2).

  Lemma join_comm:
    forall e1 e2 e3,
    Join e1 e2 e3 ->
    Join e2 e1 e3.
  Proof.
    intros.
    induction H.
    - rewrite Nat.max_comm.
      eauto using join_leaf_leaf.
    - auto using join_node_leaf.
    - auto using join_leaf_node.
    - auto.
    - apply le_lt_or_eq in H.
      destruct H. {
        apply join_node_gt, join_node_le; auto with *.
      }
      subst.
      apply join_node_le; auto;
      rewrite Nat.sub_diag in *; auto;
      rewrite lift_rw_0 in *;
      auto.
  Qed.

  Lemma join_leaf_node_le:
    forall n1 n2 l2 r2 l r,
    n1 <= n2 ->
    Join (Leaf 0) (lift l2 (n2 - n1)) l ->
    Join (Leaf 0) (lift r2 (n2 - n1)) r ->
    Join (Leaf n1) (Node n2 l2 r2) (Node n1 l r).
  Proof.
    intros.
    apply join_leaf_node, join_node_le; auto with *; auto.
  Qed.

  Lemma join_leaf_node_gt:
    forall n1 n2 l2 r2 l r,
    n1 > n2 ->
    Join (Leaf (n1-n2)) l2 l ->
    Join (Leaf (n1-n2)) r2 r ->
    Join (Leaf n1) (Node n2 l2 r2) (Node n2 l r).
  Proof.
    intros.
    apply join_comm in H0.
    apply join_comm in H1.
    apply join_leaf_node.
    apply join_node_gt; auto.
    apply join_node_le; simpl in *; auto with *; auto.
  Qed.

  Inductive Join0 : nat -> event -> event -> Prop :=
  | join0_leaf:
    forall n1 n2,
    Join0 n1 (Leaf n2) (Leaf (Nat.max n1 n2))
  | join0_node:
    forall n1 n2 r2 l2 l r,
    Join0 (n1-n2) (lift l2 (n2 - n1)) l ->
    Join0 (n1-n2) (lift r2 (n2 - n1)) r ->
    Join0 n1 (Node n2 l2 r2) (Node (Nat.min n1 n2) l r).

  Inductive Join1 : event -> event -> event -> Prop :=
  | join1_leaf_l:
    forall n1 e2 e3,
    Join0 n1 e2 e3 ->
    Join1 (Leaf n1) e2 e3
  | join1_leaf_r:
    forall e1 n2 e3,
    Join0 n2 e1 e3 ->
    Join1 e1 (Leaf n2) e3
  | join1_node:
    forall n1 n2 l1 l2 r1 r2 l r,
    Join1 (lift l1 (n1 - n2)) (lift l2 (n2 - n1)) l ->
    Join1 (lift r1 (n1 - n2)) (lift r2 (n2 - n1)) r ->
    Join1 (Node n1 l1 r1) (Node n2 l2 r2) (Node (Nat.min n1 n2) l r).

  Lemma join0_le:
    forall n1 n2 r2 l2 l r,
    n1 <= n2 ->
    Join0 0 (lift l2 (n2 - n1)) l ->
    Join0 0 (lift r2 (n2 - n1)) r ->
    Join0 n1 (Node n2 l2 r2) (Node n1 l r).
  Proof.
    intros.
    assert (R: Node n1 l r = Node (Nat.min n1 n2) l r). {
      rewrite Nat.min_l; auto.
    }
    rewrite R; clear R.
    assert (R: n1 - n2 = 0) by auto with *.
    apply join0_node; rewrite R; auto.
  Qed.

  Lemma join0_gt:
    forall n1 n2 r2 l2 l r,
    n1 > n2 ->
    Join0 (n1-n2) l2 l ->
    Join0 (n1-n2) r2 r ->
    Join0 n1 (Node n2 l2 r2) (Node n2 l r).
  Proof.
    intros.
    assert (R: Node n2 l r = Node (Nat.min n1 n2) l r). {
      rewrite Nat.min_r; auto with *.
    }
    rewrite R; clear R.
    assert (R: n2 - n1 = 0) by auto with *.
    apply join0_node; rewrite R; rewrite lift_rw_0; auto.
  Qed.

  Lemma join0_to_join:
    forall n1 e2 e3,
    Join0 n1 e2 e3 ->
    Join (Leaf n1) e2 e3.
  Proof.
    intros.
    induction H; auto using join_leaf_leaf.
    destruct (le_dec n1 n2). {
      assert (R: n1 - n2 = 0) by auto with *; rewrite R in *.
      rewrite min_l; auto using join_leaf_node, join_node_le.
    }
    assert (R: n2 - n1 = 0) by auto with *; rewrite R in *.
    rewrite lift_rw_0 in *.
    rewrite min_r; auto with *.
    apply not_le in n.
    apply join_leaf_node.
    apply join_node_gt; auto.
    apply join_node_le; auto with *; simpl; auto using join_comm.
  Qed.

  Let join_to_join0_lift:
    forall e2 n1 e3 d2,
    Join (Leaf n1) (lift e2 d2) e3 ->
    Join0 n1 (lift e2 d2) e3.
  Proof.
    induction e2; intros. {
      inversion H; subst.
      apply join0_leaf.
    }
    inversion H; subst; clear H.
    inversion H5; subst; clear H5. {
      inversion H8; subst; clear H8. {
        omega.
      }
      simpl in *.
      apply join_comm in H9.
      apply join_comm in H10.
      assert (R: e2_1 = lift e2_1 0)
      by auto using lift_rw_0; rewrite R in H9; clear R.
      assert (R: e2_2 = lift e2_2 0)
      by auto using lift_rw_0; rewrite R in H10; clear R.
      apply IHe2_1 in H9.
      apply IHe2_2 in H10.
      rewrite lift_rw_0 in *.
      apply join0_gt; auto.
    }
    apply join0_le; auto.
  Qed.

  Lemma join_to_join0:
    forall e2 n1 e3,
    Join (Leaf n1) e2 e3 ->
    Join0 n1 e2 e3.
  Proof.
    intros.
    assert (R: e2 = lift e2 0) by eauto using lift_rw_0.
    rewrite R in *.
    auto using join_to_join0_lift.
  Qed.

  Lemma join0_iff_join:
    forall n1 e2 e3,
    Join0 n1 e2 e3 <-> Join (Leaf n1) e2 e3.
  Proof.
    split; auto using join_to_join0, join0_to_join.
  Qed.

  Lemma join_node_leaf_le:
    forall n1 l1 r1 n2 l r,
    n1 <= n2 ->
    Join (Leaf (n2 - n1)) l1 l ->
    Join (Leaf (n2 - n1)) r1 r ->
    Join (Node n1 l1 r1) (Leaf n2) (Node n1 l r).
  Proof.
    intros.
    apply join_comm in H0.
    apply join_comm in H1.
    apply join_node_leaf, join_node_le; auto with *.
  Qed.

  Lemma join_node_leaf_gt:
    forall n1 l1 r1 n2 l r,
    n1 > n2 ->
    Join (Leaf 0) (lift l1 (n1-n2)) l ->
    Join (Leaf 0) (lift r1 (n1-n2)) r ->
    Join (Node n1 l1 r1) (Leaf n2) (Node n2 l r).
  Proof.
    intros.
    apply join_node_leaf,
      join_node_gt,
      join_node_le;
      simpl in *; auto with *; auto.
  Qed.

  Let join0_max_1_lift:
    forall e2 n1 d2 e3,
    Join0 n1 (lift e2 d2) e3 ->
    forall q n2,
    Interpret (Leaf n1) q n1 ->
    Interpret (lift e2 d2) q n2 ->
    Interpret e3 q (Nat.max n1 n2).
  Proof.
    induction e2; intros; simpl in *. {
      inversion H; subst; clear H.
      inversion H1; subst;
      inversion H0; subst; clear H1 H0.
      auto using interpret_leaf with *.
    }
    inversion H; subst; clear H.
    assert (Hx := IHe2_1 _ _ _ H7); clear H7 IHe2_1.
    assert (Hy := IHe2_2 _ _ _ H8); clear H8 IHe2_2.
    inversion H1; subst; clear H1. {
      remember (n + d2) as n2.
      assert (Hl: Interpret l ((2 # 1) * q) (Nat.max (n1 - n2) (n3+ (n2-n1)))). {
        eauto using interpret_lift_1, interpret_to_leaf.
      }
      destruct (le_dec n1 n2). {
        rewrite max_r in *; auto with *.
        rewrite min_l in *; auto with *.
        assert (R: n2 + n3 = n1 + (n3 + (n2 - n1))) by auto with *.
        rewrite R.
        auto using interpret_node_l.
      }
      apply not_le in n0.
      replace (n2 - n1) with 0 in * by auto with *.
      replace (n3 + 0) with n3 in * by auto with *.
      rewrite min_r in *; auto with *.
      assert (R: Nat.max n1 (n2 + n3) = n2 + (Nat.max (n1 - n2) n3)). {
        rewrite <- nat_max_add_l.
        replace (n2 + (n1 - n2)) with n1 by auto with *.
        trivial.
      }
      rewrite R.
      auto using interpret_node_l.
    }
    remember (n+d2) as n2.
    assert (Hr: Interpret r ((2 # 1) * q - 1) (Nat.max (n1 - n2) (n3+(n2-n1)))). {
      eauto using interpret_lift_1, interpret_to_leaf.
    }
    destruct (le_dec n1 n2). {
      rewrite max_r in *; auto with *.
      rewrite min_l in *; auto with *.
      assert (R: n2 + n3 = n1 + (n3 + (n2 - n1))) by auto with *.
      rewrite R.
      auto using interpret_node_r.
    }
    apply not_le in n0.
    replace (n2 - n1) with 0 in * by auto with *.
    replace (n3 + 0) with n3 in * by auto with *.
    rewrite min_r in *; auto with *.
    assert (R: Nat.max n1 (n2 + n3) = n2 + (Nat.max (n1 - n2) n3)). {
      rewrite <- nat_max_add_l.
      replace (n2 + (n1 - n2)) with n1 by auto with *.
      trivial.
    }
    rewrite R.
    auto using interpret_node_r.
  Qed.

  Let join0_max_1:
    forall e2 n1 e3,
    Join0 n1 e2 e3 ->
    forall x n2,
    Interpret (Leaf n1) x n1 ->
    Interpret e2 x n2 ->
    Interpret e3 x (Nat.max n1 n2).
  Proof.
    intros.
    replace e2 with (lift e2 0) in * by auto using lift_rw_0.
    eauto using join0_max_1_lift.
  Qed.

  Let join1_max_1_lift:
    forall e1 e2 d1 d2 e3,
    Join1 (lift e1 d1) (lift e2 d2) e3 ->
    forall x n1 n2,
    Interpret (lift e1 d1) x n1 ->
    Interpret (lift e2 d2) x n2 ->
    Interpret e3 x (Nat.max n1 n2).
  Proof.
    induction e1; intros; simpl in *. {
      inversion H0; subst; clear H0.
      inversion H; subst; clear H.
      - eauto using join0_max_1, interpret_leaf.
      - destruct e2; inversion H0; subst; clear H0.
        simpl in *.
        rewrite Nat.max_comm.
        inversion H1; subst.
        eauto using interpret_leaf.
    }
    inversion H; subst; clear H. {
      rewrite Nat.max_comm.
      destruct e2; inversion H2; subst; clear H2; simpl in *; inversion H1; subst.
      eauto.
    }
    assert (Hx := IHe1_1 _ _ _ _ H7); clear IHe1_1 H7.
    assert (Hy := IHe1_2 _ _ _ _ H8); clear IHe1_2 H8.
    destruct e2; inversion H6; subst; simpl in *; clear H6.
    inversion H0; subst; clear H0. {
      inversion H1; subst; clear H1. {
        remember (n+d1) as n1.
        remember (n0+d2) as n2.
        apply interpret_lift_1 with (d:=n1 - n2) in H6.
        apply interpret_lift_1 with (d:=n2 - n1) in H5.
        destruct (le_dec n1 n2). {
          replace (n1 - n2) with 0 in * by auto with *.
          replace (n4 + 0) with n4 in * by auto with *.
          rewrite min_l in *; auto with *.
          assert (R: Nat.max (n1 + n4) (n2 + n3) = n1 + Nat.max n4 (n3 + (n2 - n1))). {
            rewrite <- nat_max_add_l.
            replace (n1 + (n3 + (n2 - n1))) with (n2 + n3) by auto with *.
            trivial.
          }
          rewrite R.
          auto using interpret_node_l.
        }
        apply not_le in n5.
        replace (n2 - n1) with 0 in * by auto with *.
        replace (n3 + 0) with n3 in * by auto with *.
        rewrite min_r in *; auto with *.
        assert (R: Nat.max (n1 + n4) (n2 + n3) = n2 + Nat.max n3 (n4 + (n1 - n2))). {
          rewrite <- nat_max_add_l.
          replace (n2 + (n4 + (n1 - n2))) with (n1 + n4) by auto with *.
          apply Nat.max_comm.
        }
        rewrite R.
        rewrite Nat.max_comm.
        auto using interpret_node_l.
      }
      apply interpret_to_one in H6.
      apply interpret_to_one in H5.
      apply One.one_neg_1 in H6; auto; contradiction.
    }
    inversion H1; subst; clear H1. {
      apply interpret_to_one in H6.
      apply interpret_to_one in H5.
      apply One.one_neg_1 in H6; auto; contradiction.
    }
    remember (n+d1) as n1.
    remember (n0+d2) as n2.
    destruct (le_dec n1 n2). {
      replace (n1 - n2) with 0 in * by auto with *.
      rewrite min_l in *; auto with *.
      rewrite lift_rw_0 in *.
      remember (_ - 1)%Q as q.
      apply interpret_lift_1 with (d:=n2 - n1) in H5.
      assert (Interpret r q (Nat.max n4 (n3 + (n2 - n1)))) by auto.
      assert (R: Nat.max (n1 + n4) (n2 + n3) = n1 + Nat.max n4 (n3 + (n2 - n1))). {
        rewrite <- nat_max_add_l.
        replace (n1 + (n3 + (n2 - n1))) with (n2 + n3) by auto with *.
        trivial.
      }
      rewrite R.
      subst.
      auto using interpret_node_r.
    }
    apply not_le in n5.
    replace (n2 - n1) with 0 in * by auto with *.
    rewrite min_r in *; auto with *.
    rewrite lift_rw_0 in *.
    remember (_ - 1)%Q as q.
    apply interpret_lift_1 with (d:=n1 - n2) in H6.
    assert (Interpret r q (Nat.max (n4 + (n1 - n2)) n3))
    by auto.
    assert (R: Nat.max (n1 + n4) (n2 + n3) = n2 + Nat.max (n4 + (n1 - n2)) n3). {
      rewrite <- nat_max_add_l.
      replace (n2 + (n4 + (n1 - n2))) with (n1 + n4) by auto with *.
      trivial.
    }
    rewrite R.
    subst.
    auto using interpret_node_r.
  Qed.

  Lemma join1_max:
    forall e1 e2 e3,
    Join1 e1 e2 e3 ->
    forall x n1 n2,
    Interpret e1 x n1 ->
    Interpret e2 x n2 ->
    Interpret e3 x (Nat.max n1 n2).
  Proof.
    intros.
    replace e1 with (lift e1 0) in * by auto using lift_rw_0.
    replace e2 with (lift e2 0) in * by auto using lift_rw_0.
    eauto.
  Qed.

  Let join1_to_join_lift:
    forall e1 d1 e2 d2 e3,
    Join1 (lift e1 d1) (lift e2 d2) e3 ->
    Join (lift e1 d1) (lift e2 d2) e3.
  Proof.
    induction e1; intros. {
      simpl in *.
      inversion H; subst; clear H;
      auto using join0_to_join, join_comm.
    }
    simpl in *.
    inversion H; subst; clear H.
    + auto using join0_to_join, join_comm.
    + apply IHe1_1 in H5.
      apply IHe1_2 in H6.
      destruct (le_dec (n+d1) n2). {
        rewrite Nat.min_l in *; auto.
        replace (n + d1 - n2) with 0 in * by auto with *.
        rewrite lift_rw_0 in *.
        auto using join_node_le.
      }
      apply not_le in n0.
      rewrite Nat.min_r in *; auto with *.
      replace (n2 - (n + d1)) with 0 in * by auto with *.
      rewrite lift_rw_0 in *.
      auto using join_comm, join_node_gt, join_node_le with *.
  Qed.

  Lemma join1_to_join:
    forall e1 e2 e3,
    Join1 e1 e2 e3 ->
    Join e1 e2 e3.
  Proof.
    intros.
    replace e1 with (lift e1 0) in * by auto using lift_rw_0.
    replace e2 with (lift e2 0) in * by auto using lift_rw_0.
    auto.
  Qed.

  Let join0_zero_incl_lift_1:
    forall e1 e2 x n m,
    Join0 0 (lift e1 m) e2 ->
    Interpret (lift e1 m) x n ->
    Interpret e2 x n.
  Proof.
    induction e1; intros. {
      inversion H; subst; clear H.
      simpl; auto.
    }
    inversion H; subst; clear H.
    rewrite Nat.min_0_l.
    simpl in *.
    rewrite Nat.sub_0_r in *.
    inversion H0; subst; clear H0.
    - eapply IHe1_1 with (x:=((2#1)*x)%Q) in H6; eauto using interpret_lift_1.
      assert (R:n+ m + n2 = 0 + (n + m + n2)) by auto with *; rewrite R.
      apply interpret_node_l; auto.
      assert (R2:n + m + n2 = n2+ (n+ m)) by auto with *; rewrite R2.
      assumption.
    - eapply IHe1_2 with (x:=((2#1)*x-1)%Q) in H7; eauto using interpret_lift_1.
      assert (R:n+ m + n2 = 0 + (n + m + n2)) by auto with *; rewrite R.
      apply interpret_node_r; auto.
      assert (R2:n + m + n2 = n2+ (n+ m)) by auto with *; rewrite R2.
      assumption.
  Qed.

  Lemma join0_zero_1:
    forall e1 e2 x n,
    Join0 0 e1 e2 ->
    Interpret e1 x n ->
    Interpret e2 x n.
  Proof.
    intros.
    assert (R: e1 = lift e1 0) by auto using lift_rw_0.
    rewrite R in *.
    eauto using join0_zero_incl_lift_1.
  Qed.

  Let join0_zero_incl_lift_2:
    forall e1 e2 x n m,
    Join0 0 (lift e1 m) e2 ->
    Interpret e2 x n ->
    Interpret (lift e1 m) x n.
  Proof.
    induction e1; intros. {
      simpl in *.
      inversion H; subst; clear H.
      simpl in *.
      assumption.
    }
    simpl in *.
    inversion H; subst; clear H.
    rewrite Nat.sub_0_l in *.
    rewrite Nat.sub_0_r in *.
    inversion H0; subst; clear H0.
    - eapply IHe1_1 in H6; eauto; clear H7.
      apply interpret_lift_2 in H6.
      destruct H6 as (?, (?, Hi)).
      subst.
      simpl.
      assert (R: x0 + (n+m) = (n+m) + x0) by auto with *.
      rewrite R.
      auto using interpret_node_l.
    - eapply IHe1_2 in H7; eauto.
      apply interpret_lift_2 in H7.
      destruct H7 as (?, (?, Hi)).
      subst.
      simpl.
      assert (R: x0 + (n+m) = (n+m) + x0) by auto with *.
      rewrite R.
      auto using interpret_node_r.
  Qed.

  Lemma join0_zero_2:
    forall e1 e2 x n,
    Join0 0 e1 e2 ->
    Interpret e2 x n ->
    Interpret e1 x n.
  Proof.
    intros.
    assert (R: e1 = lift e1 0) by auto using lift_rw_0.
    rewrite R in *.
    eauto using join0_zero_incl_lift_2.
  Qed.

  Lemma join0_zero_iff:
    forall e1 e2 x n,
    Join0 0 e1 e2 ->
    Interpret e2 x n <-> Interpret e1 x n.
  Proof.
    split; eauto using join0_zero_1, join0_zero_2.
  Qed.

End Defs.

Section Impl.
  Require Import Recdef.

  Function join0 n1 e2 d2 { measure size e2 } : event :=
  match e2 with
  | Leaf n2 => Leaf (Nat.max n1 (n2 + d2))
  | Node n2 l2 r2 =>
    let n2 := n2 + d2 in
    let nl := n1 - n2 in
    let nr := n2 - n1 in
    let l := join0 nl l2 nr in
    let r := join0 nl r2 nr in
    Node (Nat.min n1 n2) l r
  end.
  Proof.
  - auto using size_node_r.
  - auto using size_node_l.
  Qed.

  Lemma join0_to_prop:
    forall e2 n1 d2 e3,
    join0 n1 e2 d2 = e3 ->
    Join0 n1 (lift e2 d2) e3.
  Proof.
    induction e2; intros. {
      rewrite join0_equation in *.
      rewrite <- H.
      apply join0_leaf.
    }
    rewrite join0_equation in *.
    remember (join0 _ e2_1 _).
    symmetry in Heqe.
    remember (n+d2) as n2.
    apply IHe2_1 in Heqe.
    remember (join0 _ e2_2 _).
    symmetry in Heqe0.
    apply IHe2_2 in Heqe0.
    subst.
    apply join0_node; auto.
  Qed.

  Lemma join0_from_prop:
    forall e2 n1 d2 e3,
    Join0 n1 (lift e2 d2) e3 ->
    join0 n1 e2 d2 = e3.
  Proof.
    induction e2; intros; simpl in *; rewrite join0_equation. {
      inversion H; subst; clear H.
      auto.
    }
    inversion H; subst; clear H.
    apply IHe2_1 in H5.
    apply IHe2_2 in H6.
    rewrite H5.
    rewrite H6.
    trivial.
  Qed.

  Lemma join0_spec:
    forall e2 n1 d2,
    Join (Leaf n1) (lift e2 d2) (join0 n1 e2 d2).
  Proof.
    induction e2; intros. {
      rewrite join0_equation.
      simpl.
      auto using join_leaf_leaf.
    }
    rewrite join0_equation.
    destruct (le_dec n1 (n + d2)). {
      assert (R: Nat.min n1 (n + d2) = n1) by auto with *;
      rewrite R; clear R.
      assert (R: n1 - (n + d2) = 0) by auto with *;
      rewrite R; clear R.
      apply join_leaf_node_le; auto with *.
    }
    apply not_le in n0.
    assert (R: Nat.min n1 (n + d2) = n + d2) by auto with *;
    rewrite R; clear R.
    assert (R: (n + d2) - n1 = 0) by auto with *;
    rewrite R; clear R.
    apply join_leaf_node_gt; auto with *.
    + assert (Hx := IHe2_1 (n1 - (n+d2)) 0).
      rewrite lift_rw_0 in *; auto.
    + assert (Hx := IHe2_2 (n1 - (n+d2)) 0).
      rewrite lift_rw_0 in *; auto.
  Qed.

  Function join e1 d1 e2 d2 {measure size e1 } : event :=
  match e1 with
  | Leaf n1 => join0 (n1 + d1) e2 d2
  | Node n1 l1 r1 =>
    match e2 with
    | Leaf n2 => join0 (n2 + d2) e1 d1
    | Node n2 l2 r2 =>
      let n1 := n1 + d1 in
      let n2 := n2 + d2 in
      let nl := n1 - n2 in
      let nr := n2 - n1 in
      let l := join l1 nl l2 nr in
      let r := join r1 nl r2 nr in
      Node (Nat.min n1 n2) l r
    end
  end.
  Proof.
    - auto using size_node_r.
    - auto using size_node_l.
  Qed.

  Definition join1 e1 e2 := join e1 0 e2 0.

  Lemma join_spec:
    forall e1 d1 e2 d2,
    Join (lift e1 d1) (lift e2 d2) (join e1 d1 e2 d2).
  Proof.
    induction e1; intros. {
      destruct e2;
      rewrite join_equation;
      apply join0_spec.
    }
    rewrite join_equation.
    simpl.
    destruct e2. {
      simpl.
      apply join_comm.
      assert (R: Node (n + d1) e1_1 e1_2 = lift (Node n e1_1 e1_2) d1) by auto;
      rewrite R.
      apply join0_spec.
    }
    simpl.
    remember (n+d1) as n1.
    remember (n0+d2) as n2.
    destruct (le_dec n1 n2). {
      assert (R:Nat.min n1 n2 = n1) by auto with *; rewrite R; clear R.
      assert (R: n1 - n2 = 0) by auto with *; rewrite R.
      assert (Hx := IHe1_1 0 e2_1 (n2 - n1)).
      assert (Hy := IHe1_2 0 e2_2 (n2 - n1)).
      rewrite lift_rw_0 in *.
      apply join_node_le; auto.
    }
    apply not_le in n3.
    assert (R:Nat.min n1 n2 = n2) by auto with *; rewrite R; clear R.
    assert (R: n2 - n1 = 0) by auto with *; rewrite R.
    apply join_node_gt; auto.
    assert (Hx := IHe1_1 (n1-n2) e2_1 0).
    assert (Hy := IHe1_2 (n1-n2) e2_2 0).
    rewrite lift_rw_0 in *.
    apply join_node_le; auto using join_comm with *.
  Qed.


  Goal join0 3 (Node 2 (Leaf 1) (Leaf 4)) 0 = Node 2 (Leaf 1) (Leaf 4).
    rewrite join0_equation.
    simpl.
    rewrite join0_equation.
    simpl.
    rewrite join0_equation.
    simpl.
    auto.
  Qed.

  Goal join (Leaf 4) 0 (Node 2 (Leaf 1) (Leaf 4)) 0 = Node 2 (Leaf 2) (Leaf 4).
    rewrite join_equation.
    simpl.
    rewrite join0_equation.
    simpl.
    rewrite join0_equation.
    simpl.
    rewrite join0_equation.
    simpl.
    auto.
  Qed.

  Goal join (Leaf 3) 0 (Node 2 (Leaf 3) (Leaf 3)) 0 = Node 2 (Leaf 3) (Leaf 3).
    rewrite join_equation.
    simpl.
    rewrite join0_equation.
    simpl.
    rewrite join0_equation.
    simpl.
    auto.
  Qed.


  Let join1_to_prop_lift:
    forall e1 e2 e3 d1 d2,
    join e1 d1 e2 d2 = e3 ->
    Join1 (lift e1 d1) (lift e2 d2) e3.
  Proof.
    induction e1; intros. {
      rewrite join_equation in *; simpl.
      apply join0_to_prop in H.
      auto using join1_leaf_l.
    }
    rewrite join_equation in *.
    destruct e2. {
      simpl.
      apply join0_to_prop in H.
      auto using join1_leaf_r.
    }
    remember (join e1_1 _ _ _).
    remember (join e1_2 _ _ _).
    symmetry in Heqe.
    symmetry in Heqe0.
    subst.
    eauto using join1_node.
  Qed.

  Lemma join1_to_prop:
    forall e1 e2 e3,
    join1 e1 e2 = e3 ->
    Join1 e1 e2 e3.
  Proof.
    unfold join1; intros.
    replace e1 with (lift e1 0) by auto using lift_rw_0.
    replace e2 with (lift e2 0) by auto using lift_rw_0.
    auto.
  Qed.
(*
  Lemma eval_max:
    forall x y, Clock.Max (eval x) (eval y) (eval (join1 x y)).
  Proof.
    intros.
    apply Clock.Build_Max.
    intros q.
    unfold eval.
    remember (interpret x _).
    symmetry in Heqo.
    destruct o. {
      assert (Hj: exists n2, interpret (join1 x y) q = Some n2) by eauto using interpret_rw_some.
      destruct Hj as (n2, Hj).
      rewrite Hj.
      assert (Hy: exists n3, interpret y q = Some n3) by eauto using interpret_rw_some.
      destruct Hy as (n3, Hy).
      rewrite Hy.
      apply interpret_to_prop in Heqo.
      apply interpret_to_prop in Hj.
      apply interpret_to_prop in Hy.
      remember (join1 x y).
      symmetry in Heqe.
      apply join1_to_prop in Heqe.
      eapply join1_max in Heqe; eauto.
      eauto using interpret_fun.
    }
    repeat rewrite interpret_rw_none with (x:=x); auto.
  Qed.
  *)
End Impl.