Require Import Coq.QArith.QArith.
Require Import Coq.micromega.Psatz.

Section Defs.

  Inductive One: Q -> Prop :=
  | one_def:
    forall x,
    x >= 0 ->
    x < 1 ->
    One x.

  Definition one (x:Q) : bool := 
  if Qlt_le_dec x 0 then (* x < 0 *) false
  (* x >= 0 *)
  else if Qlt_le_dec x 1
  then true (* x >= 0 /\ x < 1 *)
  else false.

  Lemma one_to_true:
    forall x,
    One x ->
    one x = true.
  Proof.
    intros.
    unfold one.
    destruct (Qlt_le_dec x 0). {
      inversion H; subst.
      lra.
    }
    destruct (Qlt_le_dec x 1). {
      auto using one_def.
    }
    inversion H; lra.
  Qed.

  Lemma one_inv_0:
    forall x,
    one x = false ->
    x < 0 \/ x >= 1.
  Proof.
    unfold one; intros.
    destruct (Qlt_le_dec x 0); auto.
    destruct (Qlt_le_dec x 1); auto.
    inversion H.
  Qed.

  Lemma one_to_prop:
    forall x,
    one x = true ->
    One x.
  Proof.
    unfold one; intros.
    destruct (Qlt_le_dec x 0); auto; try inversion H.
    destruct (Qlt_le_dec x 1); auto; try inversion H.
    auto using one_def.
  Qed.

  Lemma one_dec x : { One x } + { ~ One x }.
  Proof.
    remember (one x).
    symmetry in Heqb.
    destruct b. {
      auto using one_to_prop.
    }
    right.
    unfold not; intros.
    apply one_to_true in H.
    rewrite H in *.
    inversion Heqb.
  Defined.

  Lemma one_split_1:
    forall x,
    One ((2 # 1) * x) ->
    One x.
  Proof.
    intros.
    inversion H; subst.
    apply one_def; lra.
  Qed.

  Lemma one_split_2:
    forall x,
    One ((2 # 1) * x - 1) ->
    One x.
  Proof.
    intros.
    inversion H; subst.
    apply one_def; lra.
  Qed.

  Lemma one_split_3:
    forall x,
    One x ->
    One ((2 # 1) * x) \/ One ((2 # 1) * x - 1).
  Proof.
    intros.
    inversion H; subst.
    destruct (Qlt_le_dec x ((1#2))). {
      left; apply one_def; lra.
    }
    right; apply one_def; lra.
  Qed.

  Lemma one_neg_1:
    forall x,
    One ((2 # 1) * x) -> ~ One ((2 # 1) * x - 1).
  Proof.
    unfold not; intros;
    inversion H; inversion H0; lra.
  Qed.

  Lemma one_neg_2:
    forall x,
    One ((2 # 1) * x) -> ~ One ((2 # 1) * x - 1).
  Proof.
    unfold not; intros;
    inversion H; inversion H0; lra.
  Qed.

  Lemma one_exists:
    exists x, One x.
  Proof.
    exists 0.
    auto using one_def with *.
  Qed.

  Lemma one_rw:
    forall x y,
    x == y ->
    One x ->
    One y.
  Proof.
    intros.
    inversion H0; subst.
    apply one_def; lra.
  Qed.

End Defs.

