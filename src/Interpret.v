Require Import Coq.micromega.Psatz.
Require Import Coq.Arith.Wf_nat.
Require Import Coq.Bool.Bool.
Require Import Coq.Arith.Compare_dec.
Require Import Recdef.
Require Import Coq.Setoids.Setoid.
Require Import Coq.QArith.QArith.
Require Import Clock.
Require Import IntervalTreeMap.

Open Scope nat_scope.

Section Defs.

  Definition to_nat (b:bool) :=
  match b with
  | true => 1
  | false => 0
  end.

  Require One.

  Inductive Interpret : event -> Q -> nat -> Prop :=
  | interpret_leaf:
    forall x n,
    One.One x -> 
    Interpret (Leaf n) x n
  | interpret_node_l:
    forall x n1 n2 l r,
    Interpret l ((2#1) * x) n2 ->
    Interpret (Node n1 l r) x (n1 + n2)%nat
  | interpret_node_r:
    forall x n1 n2 l r,
    Interpret r ((2#1) * x - 1) n2 ->
    Interpret (Node n1 l r) x (n1 + n2)%nat.

  Lemma interpret_to_one:
    forall e x n,
    Interpret e x n ->
    One.One x.
  Proof.
    intros.
    induction H;
    auto using One.one_split_1, One.one_split_2.
  Qed.

  Lemma one_to_interpret:
    forall e x,
    One.One x ->
    exists n, Interpret e x n.
  Proof.
    induction e; intros. {
      eauto using interpret_leaf.
    }
    apply One.one_split_3 in H.
    destruct H. {
      apply IHe1 in H.
      destruct H.
      eauto using interpret_node_l.
    }
    apply IHe2 in H.
    destruct H.
    eauto using interpret_node_r.
  Qed.

  Lemma interpret_absurd:
    forall x n1 n2 e1 e2,
    Interpret e1 ((2 # 1) * x) n1 ->
    ~ Interpret e2 ((2 # 1) * x - 1) n2.
  Proof.
    intros.
    unfold not; intros.
    apply interpret_to_one in H.
    apply interpret_to_one in H0.
    apply One.one_neg_1 in H.
    contradiction.
  Qed.

  Lemma interpret_fun:
    forall e x n1 n2,
    Interpret e x n1 ->
    Interpret e x n2 ->
    n1 = n2.
  Proof.
    induction e; intros; simpl. {
      inversion H; subst; clear H.
      inversion H0; subst; clear H0; auto.
    }
    inversion H; subst; clear H. {
      inversion H0; subst; clear H0; eauto.
      eapply interpret_absurd in H5; eauto; contradiction.
    }
    inversion H0; subst; clear H0. {
      eapply interpret_absurd in H6; eauto.
      contradiction.
    }
    eauto.
  Qed.

  Lemma nat_max_add_l:
    forall z x y,
    Nat.max (z + x) (z + y) = z + Nat.max x y.
  Proof.
    induction z; intros. {
      auto.
    }
    simpl.
    rewrite IHz.
    auto.
  Qed.

  Let nat_max_add_r:
    forall z x y,
    Nat.max (x + z) (y + z) = Nat.max x y + z.
  Proof.
    induction z; intros. {
      assert (r1: x + 0 = x) by auto with *.
      assert (r2: y + 0 = y) by auto with *.
      rewrite r1; rewrite r2.
      auto with *.
    }
    assert (r1: x + S z = S (x + z)) by auto with *.
    assert (r2: y + S z = S (y + z)) by auto with *.
    rewrite r1; rewrite r2.
    simpl.
    rewrite IHz.
    auto with *.
  Qed.

  Let nat_max_mul_r:
    forall x y z,
    Nat.max (x * z) (y * z) = Nat.max x y * z.
  Proof.
    induction x; intros. {
      simpl.
      auto.
    }
    simpl.
    destruct y. {
      simpl in *.
      rewrite Nat.max_comm.
      auto.
    }
    simpl.
    rewrite nat_max_add_l.
    rewrite IHx.
    trivial.
  Qed.

  Let nat_max_mul_l:
    forall x y z,
    Nat.max (z * x) (z * y) = z * Nat.max x y.
  Proof.
    intros.
    assert (R: z * x = x * z) by auto with *; rewrite R; clear R.
    assert (R: z * y = y * z) by auto with *; rewrite R; clear R.
    rewrite nat_max_mul_r.
    auto with *.
  Qed.

  Let mul_0:
    forall x y,
    x * S y = x + x * y.
  Proof.
    intros.
    induction x. {
      auto.
    }
    simpl.
    rewrite IHx.
    auto with *.
  Qed.

  Let le_0:
    forall z x y,
    x <= y ->
    x * z <= y * z.
  Proof.
    induction z; intros. {
      auto with *.
    }
    rewrite mul_0.
    rewrite mul_0.
    assert (Hx : x * z <= y * z) by auto.
    auto with *.
  Qed.

  Lemma interpret_lift_1:
    forall e x n d,
    Interpret e x n ->
    Interpret (lift e d) x (n + d).
  Proof.
    intros.
    inversion H; subst; simpl.
    + auto using interpret_leaf.
    + assert (R: n1 + n2 + d = (n1 + d) + n2) by auto with *.
      rewrite R.
      auto using interpret_node_l.
    + assert (R: n1 + n2 + d = (n1 + d) + n2) by auto with *.
      rewrite R.
      auto using interpret_node_r.
  Qed.

  Lemma interpret_lift_2:
    forall e x n d,
    Interpret (lift e d) x n ->
    exists m, n = m + d /\ Interpret e x m.
  Proof.
    intros.
    inversion H; subst; clear H.
    - destruct e; simpl in *; inversion H0.
      subst.
      eauto using interpret_leaf.
    - destruct e; simpl in *; inversion H0; subst.
      exists (n + n2); split; auto with *.
      eauto using interpret_node_l.
    - destruct e; simpl in *; inversion H0; subst.
      exists (n + n2); split; auto with *.
      eauto using interpret_node_r.
  Qed.

  Lemma interpret_to_leaf:
    forall x q n1 n2,
    Interpret x q n1 ->
    Interpret (Leaf n2) q n2.
  Proof.
    intros.
    apply interpret_to_one in H.
    auto using interpret_leaf.
  Qed.

  Let interpret_exists_0:
    forall e x,
    (0 <= x)%Q ->
    (x < 1)%Q ->
    exists n, Interpret e x n.
  Proof.
    induction e; intros. {
      eauto using interpret_leaf, One.one_def.
    }
    destruct (Qlt_le_dec x (1#2)). {
      assert (Hi: exists n1, Interpret e1 ((2 # 1) * x) n1). {
        apply IHe1; lra.
      }
      destruct Hi as (n1, Hi).
      eauto using interpret_node_l.
    }
    assert (Hi: exists n2, Interpret e2 ((2 # 1) * x - 1) n2). {
      apply IHe2; lra.
    }
    destruct Hi as (n1, Hi).
    eauto using interpret_node_r.
  Qed.

  Lemma interpret_exists:
    forall e,
    exists x n, Interpret e x n.
  Proof.
    intros.
    exists 0%Q.
    eauto with *.
  Qed.

  Lemma interpret_inv_l:
    forall n1 n2 n3 l r q e,
    Interpret (Node n1 l r) q n2 -> 
    Interpret e ((2 # 1) * q) n3 ->
    exists n4, Interpret l ((2 # 1) * q) n4 /\ n2 = n1 + n4.
  Proof.
    intros.
    inversion H; subst; clear H. {
      eauto.
    }
    apply interpret_absurd with (n2:=n4) (e2:=r) in H0.
    contradiction.
  Qed.

  Lemma interpret_inv_r:
    forall n1 n2 n3 l r q e,
    Interpret (Node n1 l r) q n2 -> 
    Interpret e ((2 # 1) * q - 1) n3 ->
    exists n4, Interpret r ((2 # 1) * q - 1) n4 /\ n2 = n1 + n4.
  Proof.
    intros.
    inversion H; subst; clear H. {
      apply interpret_absurd with (n2:=n3) (e2:=e) in H6.
      contradiction.
    }
    eauto.
  Qed.

  Lemma interpret_inv_eq_l:
    forall n1 n2 n3 l r q,
    Interpret (Node n1 l r) q n2 ->
    Interpret l ((2 # 1) * q) n3 ->
    n2 = n1 + n3.
  Proof.
    intros.
    apply interpret_inv_l with (e:=l) (n3:=n3) in H; auto.
    destruct H as (n4, (?,Hx)).
    subst.
    eauto using interpret_fun.
  Qed.

  Lemma interpret_inv_eq_r:
    forall n1 n2 n3 l r q,
    Interpret (Node n1 l r) q n2 ->
    Interpret r ((2 # 1) * q - 1) n3 ->
    n2 = n1 + n3.
  Proof.
    intros.
    apply interpret_inv_r with (e:=r) (n3:=n3) in H; auto.
    destruct H as (n4, (?,Hx)).
    subst.
    eauto using interpret_fun.
  Qed.
(*
  Global Instance IMap e : Map (Interpret e).
  Proof.
    eauto using interpret_fun, Build_Map.
  Qed.

  Let interpret_not_le_lift:
    forall e,
    ~  Le (Interpret (lift e 1)) (Interpret e).
  Proof.
    intros.
    unfold not; intros N.
    destruct (interpret_exists (lift e 1)) as (x, (n1, Hi)).
    assert (Hj := Hi).
    apply interpret_lift_2 in Hi.
    destruct Hi as (ni,(?,Hi)).
    subst.
    inversion N; subst; clear N.
    apply H in Hj.
    destruct Hj as (nj, (Hj,?)).
    replace nj with ni in * by eauto using map_fun.
    omega.
  Qed.
  *)
(*
  Let interpret_le_inv_node:
    forall l1 l2 n1 n2 r1 r2,
    Le (Interpret (Node n1 l1 r1)) (Interpret (Node n2 l2 r2)) ->
    Le (Interpret l1) (Interpret l2) \/ Le (Interpret l1) (Interpret l2).
  Proof.
    intros.
    inversion H; subst; clear H.
    destruct (interpret_exists (Node n1 l1 r1)) as (x, (n, Hi)).
    inversion Hi; subst. {
      left.
      apply H0 in Hi.
      destruct Hi as (?, (?,?)).
      apply le_def; intros.
      inversion H; subst; clear H. {
        exists n5.
        replace n3 with n5 in * by eauto using map_fun.
      }
    } 
    apply le_def; intros.
    assert (Interpret (Node n1 l1 r1) x (n1 + n0)). {
      apply interpret_node_l.
    }
  Qed.
*)

  Lemma interpret_rw:
    forall e x y n,
    x == y ->
    Interpret e x n ->
    Interpret e y n.
  Proof.
    induction e; intros. {
      inversion H0; subst; clear H0.
      apply interpret_leaf.
      eauto using One.one_rw.
    }
    inversion H0; subst; clear H0.
    - apply interpret_node_l.
      apply IHe1 with (y:=((2#1)*y)%Q) in H6; auto; lra.
    - apply interpret_node_r.
      apply IHe2 with (y:=((2#1)*y -1)%Q) in H6; auto; lra.
  Qed.

  Let lift_lt_1:
    forall e x n1,
    Interpret e x n1 ->
    exists n2, Interpret (lift e 1) x n2 /\ n1 < n2.
  Proof.
    intros.
    destruct e. {
      simpl.
      inversion H.
      eauto using interpret_leaf with *.
    }
    simpl.
    inversion H; subst; clear H. {
      eauto using interpret_node_l with *.
    }
    eauto using interpret_node_r with *.
  Qed.

  Lemma interpret_node_l_1:
    forall l r x n1 n2,
    Interpret l x n2 ->
    Interpret (Node n1 l r) ((1 # 2) * x) (n1 + n2).
  Proof.
    intros.
    apply interpret_node_l.
    apply interpret_rw with (x:=x); try lra; auto.
  Qed.

  Lemma interpret_node_r_1:
    forall l r x n1 n2,
    Interpret r x n2 ->
    Interpret (Node n1 l r) ((1 # 2) * x + (1#2)) (n1 + n2).
  Proof.
    intros.
    apply interpret_node_r.
    apply interpret_rw with (x:=x); try lra; auto.
  Qed.

  Fixpoint min (e:event) : nat :=
  match e with
  | Leaf n => n
  | Node n l r => (Nat.min (min l) (min r))
  end.

  Fixpoint max (e:event) : nat :=
  match e with
  | Leaf n => n
  | Node n l r => n + (Nat.max (max l) (max r))
  end.

  Require One.
  Require Pid.

End Defs.

Section Impl.
  Fixpoint interpret e (x:Q) :=
  match e with
  | Leaf n => 
    if One.one_dec x then Some n else None
  | Node n e1 e2 => 
    if One.one_dec x then
    match interpret e1 ((2#1)*x), interpret e2 ((2#1)*x - 1) with
    | Some n1, _ => Some (n + n1)
    | _, Some n2 => Some (n + n2)
    | _, _ => None
    end
    else None
  end.

  Lemma interpret_to_prop:
    forall e x n,
    interpret e x = Some n ->
    Interpret e x n.
  Proof.
    induction e; intros. {
      simpl in *.
      destruct (One.one_dec x). {
        inversion H; subst; clear H.
        auto using interpret_leaf.
      }
      inversion H.
    }
    simpl in *.
    destruct (One.one_dec x). {
      remember (interpret e1 _).
      destruct o0. {
        inversion H; subst; clear H.
        symmetry in Heqo0.
        auto using interpret_node_l.
      }
      symmetry in Heqo0.
      remember (interpret e2 _).
      destruct o0. {
        inversion H; subst; clear H.
        symmetry in Heqo0.
        auto using interpret_node_r.
      }
      inversion H.
    }
    inversion H.
  Qed.

  Lemma interpret_to_some:
    forall e x n,
    Interpret e x n ->
    interpret e x = Some n.
  Proof.
    intros.
    induction H; simpl.
    - destruct (One.one_dec x); auto.
      contradiction.
    - destruct (One.one_dec x). {
        rewrite IHInterpret.
        auto.
      }
      contradiction n.
      apply interpret_to_one in H.
      auto using One.one_split_1.
    - destruct (One.one_dec x). {
        remember (interpret l _).
        symmetry in Heqo0.
        destruct o0. {
          apply interpret_to_prop in Heqo0.
          apply interpret_to_one in H.
          apply interpret_to_one in Heqo0.
          apply One.one_neg_1 in H; auto; contradiction.
        }
        rewrite IHInterpret.
        auto.
      }
      contradiction n.
      eauto using interpret_to_one, One.one_split_2.
  Qed.

  Lemma interpret_dec e x:
    { exists n, Interpret e x n } + { forall n, ~ Interpret e x n }.
  Proof.
    remember (interpret e x).
    symmetry in Heqo.
    destruct o. {
      eauto using interpret_to_prop.
    }
    right.
    unfold not; intros.
    apply interpret_to_some in H.
    rewrite  H in *.
    inversion  Heqo.
  Defined.

  Definition eval e x :=
  match (interpret e x) with
  | Some m => m
  | _ => 0
  end.

  Lemma mul_dist:
    forall z x y,
    (x + y) * z = x * z + y * z.
  Proof.
    auto with *.
  Qed.
(*
  Lemma interpret_lift:
    forall e n x,
    n * (to_nat (One.one x))  + interpret e x = interpret (lift e n) x.
  Proof.
    induction e; intros. {
      simpl.
      rewrite <- mul_dist.
      assert (R: n+n0 = n0 + n) by auto with *.
      rewrite R.
      auto.
    }
    simpl.
    remember (_ * x) % Q.
    remember (to_nat _).
    assert (R:
      n0 * n1 + (n * n1 + interpret e1 q + interpret e2 (q - 1)) =
      (n0 * n1 + n * n1) + (interpret e1 q + interpret e2 (q - 1)))
    by auto with *; rewrite R; clear R.
    assert (R:
      (n + n0) * n1 + interpret e1 q + interpret e2 (q - 1)
      = ((n + n0) * n1) + (interpret e1 q + interpret e2 (q - 1)))
    by auto with *; rewrite R; clear R.
    rewrite mul_dist.
    assert (R:  n0*n1 + n*n1 = n*n1 + n0*n1) by auto with *;
    rewrite R; auto.
  Qed.
*)
  Fixpoint size e :=
  match e with
  | Leaf _ => 1
  | Node _ l r => 1 + size l + size r
  end.

  Let size_lift_rw:
    forall e n,
    size (lift e n) = size e.
  Proof.
    intros.
    destruct e; simpl in *; auto.
  Qed.

  Let size_invariant:
    forall e,
    size e >= 1.
  Proof.
    intros; destruct e; auto.
    simpl.
    intuition.
  Qed.

  Lemma size_node_r:
    forall r n l,
    size r < size (Node n l r).
  Proof.
    induction r; intros. {
      simpl.
      assert (size l >= 1) by auto.
      intuition.
    }
    simpl.
    intuition.
  Qed.

  Lemma size_node_l:
    forall r n l,
    size l < size (Node n l r).
  Proof.
    induction l; intros. {
      simpl.
      assert (size r >= 1) by auto.
      intuition.
    }
    simpl.
    intuition.
  Qed.

  Lemma size_lift_r:
    forall n l r m,
    size (lift r m) < size (Node n l r).
  Proof.
    intros.
    destruct r; simpl; intuition; auto.
  Qed.

  Lemma size_lift_l:
    forall n l r m,
    size (lift l m) < size (Node n l r).
  Proof.
    intros.
    destruct l; simpl; intuition; auto.
  Qed.

  Goal interpret (Node 1 (Leaf 0) (Leaf 2)) (1#2) = Some 3.
    auto.
  Qed.
  
  Goal
    let x := (Node 1 (Leaf 0) (Leaf 2)) in
    let y := (Node 0 x (Leaf 0)) in
    interpret y (1#6) = Some 1.
  Proof.
    simpl.
    auto.
  Qed.

  Goal interpret (Node 2 (Leaf 1) (Leaf 1)) (2#3) = Some 3.
    simpl.
    auto.
  Qed.

  Lemma interpret_none:
    forall e x,
    interpret e x = None ->
    forall n, ~ Interpret e x n.
  Proof.
    intros.
    unfold not; intros.
    apply interpret_to_some in H0.
    rewrite H0 in *.
    inversion H.
  Qed.

  Lemma interpret_rw_some:
    forall x q n1 y,
    interpret x q = Some n1 ->
    exists n2, interpret y q = Some n2.
  Proof.
    intros.
    apply interpret_to_prop in H.
    apply interpret_to_one in H.
    apply (one_to_interpret y) in H.
    destruct H as (n, H).
    eauto using interpret_to_some.
  Qed.

  Lemma interpret_rw_none:
    forall x q y,
    interpret x q = None ->
    interpret y q = None.
  Proof.
    intros.
    remember (interpret y q).
    destruct o. {
      symmetry in Heqo.
      assert (Hx: exists n1, interpret x q = Some n1) by eauto using interpret_rw_some.
      destruct Hx as (?, R).
      rewrite R in *.
      inversion H.
    }
    trivial.
  Qed.
(*
  Instance ITC : @Clock event Pid.pid eval Pid.Interpret join1 inc.
  Proof.
    apply Build_Clock.
    + intros.
      apply Build_Max.
      intros q.
      unfold eval.
      remember (interpret x _).
      symmetry in Heqo.
      destruct o. {
        assert (Hj: exists n2, interpret (join1 x y) q = Some n2) by eauto.
        destruct Hj as (n2, Hj).
        rewrite Hj.
        assert (Hy: exists n3, interpret y q = Some n3) by eauto.
        destruct Hy as (n3, Hy).
        rewrite Hy.
        apply interpret_to_prop in Heqo.
        apply interpret_to_prop in Hj.
        apply interpret_to_prop in Hy.
        remember (join1 x y).
        symmetry in Heqe.
        apply join1_to_prop in Heqe.
        eapply join1_max in Heqe; eauto.
        eauto using interpret_fun.
      }
      repeat rewrite interpret_rw_none with (x:=x); auto.
    + intros.
  Qed.
*)

(*
  Fixpoint fill i e :=
  match i, e with
  | Pid.Leaf One.OFF, _ => Some e
  | Pid.Leaf One.ON, _ => Some (Leaf (max e))
  | Pid.Node (Pid.Leaf One.ON) ir, Node n el er =>
      match fill ir er with
      | Some er => Some (Node n (Leaf (Nat.max (max el) (min er))) er)
      | _ => None
      end
  | Pid.Node il (Pid.Leaf One.ON), Node n el er =>
      match fill il el with
      | Some el => Some (Node n el (Leaf (Nat.max (max er) (min el))))
      | _ => None
      end
  | Pid.Node il ir, Node n el er =>
      match fill il el, fill ir el with
      | Some el, Some er => Some (Node n el er)
      | _,_ => None
      end
  | _, _ => None
  end.

  Goal
    let i := (Pid.Node (Pid.Leaf One.OFF) (Pid.Node (Pid.Leaf One.ON) (Pid.Leaf One.OFF))) in
   (fill i
    (Node 3 (Node 1 (Leaf 3) (Leaf 2)) (Leaf 4))) =
    Some (Node 3 (Node 1 (Leaf 3) (Leaf 2)) (Node 1 (Leaf 3) (Leaf 2))).
  Proof.
    intros.
    simpl.
    auto.
  Qed.

  Goal norm
   (Node 3 (Node 1 (Leaf 3) (Leaf 2)) (Leaf 4)) =
   Node 6 (Node 0 (Leaf 1) (Leaf 0)) (Leaf 1).
  Proof.
    simpl.
    auto.
  Qed.

  Goal
    let i := (Pid.Node (Pid.Leaf One.OFF) (Pid.Node (Pid.Leaf One.ON) (Pid.Leaf One.OFF))) in
   (fill i
   (Node 6 (Node 0 (Leaf 1) (Leaf 0)) (Leaf 1)))
     =
    Some (Node 6 (Node 0 (Leaf 1) (Leaf 0)) (Node 0 (Leaf 1) (Leaf 0))).
  Proof.
    intros.
    simpl.
    auto.
  Qed.

  Goal
    let i := (Pid.Node (Pid.Leaf One.OFF) (Pid.Node (Pid.Leaf One.ON) (Pid.Leaf One.OFF))) in
   (fill i
   (Node 6 (Node 0 (Leaf 1) (Leaf 0)) (Node 0 (Leaf 1) (Leaf 0))))
     =
    Some (Node 6 (Node 0 (Leaf 1) (Leaf 0)) (Node 0 (Leaf 1) (Leaf 0))).
  Proof.
    simpl.
    auto.
  Qed.*)
End Impl.