Require Import Coq.Arith.Arith.
Require Import Coq.omega.Omega.
Require Import Coq.QArith.QArith.
Require Import One.
Require Import Interpret.
Require Import IntervalTreeMap.

Section Defs.
  Inductive Advance : Pid.pid -> event -> event -> Prop :=
  | advance_leaf:
    forall e,
    Advance (Pid.Leaf Pid.ONE) e (lift e 1)
  | advance_node_l:
    forall il ir n el er el',
    Advance il el el' ->
    Advance (Pid.Node il ir) (Node n el er) (Node n el' er)
  | advance_node_r:
    forall il ir n el er er',
    Advance ir er er' ->
    Advance (Pid.Node il ir) (Node n el er) (Node n el er').

  Let advance_lt_1:
    forall i e1 e2,
    Advance i e1 e2 ->
    exists x n1,
    Interpret e1 x n1 /\
    exists n2,
    Interpret e2 x n2  /\
    n1 < n2.
  Proof.
    intros.
    induction H.
    - destruct (interpret_exists e) as (x, (n, Hi)).
      exists x; exists n;
      eauto using interpret_lift_1 with *.
    - destruct IHAdvance as (x, (n1, (Hi, (n2, (Hj,?))))).
      exists ((1#2)*x)%Q.
      exists (n+n1).
      split; eauto using interpret_node_l_1 with *.
    - destruct IHAdvance as (x, (n1, (Hi, (n2, (Hj,?))))).
      exists ((1#2)*x + (1#2))%Q.
      exists (n+n1).
      split; eauto using interpret_node_r_1 with *.
  Qed.

  Fixpoint advance i e :=
  match i, e with
  | Pid.Leaf Pid.ZERO, _ => None
  | Pid.Leaf Pid.ONE, _ => Some (lift e 1)
  | Pid.Node il ir, Node n el er =>
    match advance il el, advance ir er with
    | Some el, _ => Some (Node n el er)
    | None, Some er => Some (Node n el er)
    | _, _ => None
    end
  | _, _ => None
  end.

  Lemma advance_to_prop:
    forall i e1 e2,
    advance i e1 = Some e2 ->
    Advance i e1 e2.
  Proof.
    induction i; intros; simpl in *. {
      destruct b. {
        inversion H; subst.
        auto using advance_leaf.
      }
      inversion H.
    }
    destruct e1. {
      inversion H.
    }
    remember (advance i1 _).
    symmetry in Heqo.
    destruct o. {
      inversion H; subst.
      auto using advance_node_l.
    }
    remember (advance i2 _).
    symmetry in Heqo0.
    destruct o. {
      inversion H; subst.
      auto using advance_node_r.
    }
    inversion H.
  Qed.

  Require Clock.

  Class Le x y := {
    le_1: forall q n1 n2,
    Interpret x q n1 ->
    Interpret y q n2 ->
    n1 <= n2;
  }.

  Let le_node_l:
    forall l1 l2 r n,
    Le l1 l2 ->
    Le (Node n l1 r) (Node n l2 r).
  Proof.
    intros.
    apply Build_Le; intros.
    inversion H0; subst; clear H0. {
      eapply interpret_inv_l in H1; eauto.
      destruct H1 as (?, (Hl2, ?)).
      subst.
      assert (n3 <= x) by eauto using le_1.
      auto with *.
    }
    eapply interpret_inv_r in H1; eauto.
    destruct H1 as (?, (Hl2, ?)).
    subst.
    assert (n3 = x) by eauto using interpret_fun; subst.
    auto with *.
  Qed.

  Let le_node_r:
    forall r1 r2 l n,
    Le r1 r2 ->
    Le (Node n l r1) (Node n l r2).
  Proof.
    intros.
    apply Build_Le; intros.
    inversion H0; subst; clear H0. {
      eapply interpret_inv_l in H1; eauto.
      destruct H1 as (?, (Hl2, ?)).
      subst.
      assert (n3 = x) by eauto using interpret_fun; subst.
      auto with *.
    }
    eapply interpret_inv_r in H1; eauto.
    destruct H1 as (?, (Hl2, ?)).
    subst.
    assert (n3 <= x) by eauto using le_1.
    auto with *.
  Qed.

  Let le_leaf:
    forall n1 n2,
    n1 <= n2 ->
    Le (Leaf n1) (Leaf n2).
  Proof.
    intros.
    apply Build_Le; intros.
    inversion H0; subst; clear H0.
    inversion H1; subst; clear H1.
    assumption.
  Qed.

  Let advance_le:
    forall e1 i e2,
    Advance i e1 e2 ->
    Le e1 e2.
  Proof.
    induction e1; intros. {
      inversion H; subst; clear H.
      simpl in *.
      auto with *.
    }
    inversion H; subst; clear H.
    + apply Build_Le; intros; simpl in *.
      inversion H; subst; clear H; simpl. {
        eapply interpret_inv_eq_l in H0; eauto.
        subst.
        auto with *.
      }
      eapply interpret_inv_eq_r in H0; eauto.
      subst.
      auto with *.
    + eauto.
    + eauto.
  Qed.

  Record timestamp := {
    timestamp_id: Pid.pid;
    timestamp_time: event;
  }.

  Require Rel.

  Import Rel.ITC_LE2.

  Let Lt := Clock.LeToLt.lt Le2.

  Let hb (x y:timestamp) := Lt (timestamp_time x) (timestamp_time y).

  Inductive Tick (x:timestamp) : timestamp -> Prop :=
  | tick_def:
    forall e,
    Advance (timestamp_id x) (timestamp_time x) e ->
    Tick x {|timestamp_id:=timestamp_id x; timestamp_time:=e|}.


  Let lt_1:
    forall x y i,
    Le2 x y ->
    Advance i x y ->
    Lt x y.
  Proof.
    intros.
    apply Clock.LeToLt.lt_def; auto.
    unfold not; intros N.
    unfold not; intros.
    assert (Eq f g) by auto using le_to_eq.
    assert (f x = g x). {
      auto using eq_1.
    }
    subst.
    auto with *.
  Qed.

  Let advance_to_hb:
    forall x i y dx dy,
    Advance i (lift x dx) (lift y dy) ->
    Lt (lift x dx) (lift y dy).
  Proof.
    induction x; intros. {
      inversion H; subst.
      apply lt_leaf.
      auto with *.
    }
    inversion H; subst; clear H; simpl.
    - apply lt_node; auto with *.
      + 
  Qed.
    
  Let tick_to_hb:
    forall x y,
    Tick x y ->
    hb x y.
  Proof.
    unfold hb.
    intros.
    
    inversion H.
    apply Clock.LeToLt.lt_def.
    - unfold le.
      simpl.
      eauto.
    - 
  Qed.

(*
  Let le_to_clock_le:
    forall e1 e2,
    Le e1 e2 ->
    Clock.Le (eval e1) (eval e2).
  Proof.
    intros.
    apply Clock.Build_Le; intros.
    unfold eval.
    remember (interpret e1 x).
    symmetry in Heqo.
    destruct o. {
      assert (Hx:=Heqo).
      apply interpret_rw_some with (y:=e2) in Hx.
      destruct Hx as (n2, R).
      rewrite R.
      apply interpret_to_prop in Heqo.
      apply interpret_to_prop in R.
      eauto using le_1.
    }
    rewrite interpret_rw_none with (x:=e1); auto.
  Qed.

  Let clock_lt_0:
    forall i e1 e2,
    Advance i e1 e2 ->
    Clock.Lt (eval e1) (eval e2).
  Proof.
    intros.
    destruct (advance_lt_1 _ _ _ H) as (x, (n, (Hx,(?,(Hy,?))))).
    apply Clock.lt_1 with (x:=x); eauto.
    apply interpret_to_some in Hx.
    apply interpret_to_some in Hy.
    unfold eval.
    rewrite Hx.
    rewrite Hy.
    assumption.
  Qed.
*)
(*
  (** Function advance follows the spec. *)

  Lemma advance_lt:
    forall i e1 e2,
    advance i e1 = Some e2 ->
    Clock.Lt (eval e1) (eval e2).
  Proof.
    intros.
    apply advance_to_prop in H.
    eauto.
  Qed.
  *)
(*
  Let advance_to_interpret:
    forall i e1 e2 q n,
    Advance i e1 e2 ->
    Interpret e1 q n ->
    Pid.Interpret i q.
  Proof.
    induction i; intros. {
      inversion H; subst; clear H.
      eauto using interpret_to_one, Pid.interpret_leaf.
    }
    inversion H; subst; clear H.
    - inversion H0; subst; clear H0. {
        eauto using Pid.interpret_node_l.
      }
      apply Pid.interpret_node_l.
      eauto using Pid.interpret_node_l.
    - inversion H0.
  Qed.

  Let advance_spec_2:
    forall i x y n1 n2 q,
    Advance i x y ->
    ~ Pid.Interpret i q ->
    Interpret x q n1 ->
    Interpret y q n2 ->
    n1 = n2.
  Proof.
    induction i; intros. {
      (* absurd case *)
      inversion H; subst; clear H.
      contradiction H0.
      eauto using interpret_to_one, Pid.interpret_leaf.
    }
    inversion H; subst; clear H. {
      inversion H2; subst; clear H2. {
        eapply interpret_inv_l in H1; eauto.
        destruct H1 as (ni, (Hi, ?)).
        subst.
        contradiction H0.
        apply Pid.interpret_node_l.
      }
    }
  Qed.
*)
(*
  Lemma
    forall i x y,
    advance i x = Some y->
    
    Eq (restrict (p i) (eval x)) (restrict (p i) (eval y))
*)

End Defs.
