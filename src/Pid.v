Require Import Coq.QArith.QArith.
Require Import Coq.micromega.Psatz.
Require Import One.

Section Defs.


  (** An identity tree represents a collection of intervals, each of
    which splits an interval into 0.5.
    For instance, [Node ZERO ONEE] corresponds to the interval [0.5,1)
    And [Node (Node ONE ZERO) (Node ONE ZERO)] corresponds to the intervals
    [0,0.25) and [0.5,0.25).
    *)

  Inductive bit := ONE | ZERO.
  Definition bit_eq (x:bit) (y:bit) : { x = y } + { x <> y }.
  Proof.
    destruct x, y; auto;
    right; intros N; inversion N.
  Defined.

  Inductive pid := Leaf: bit -> pid | Node: pid -> pid -> pid.

  Inductive Nonzero : pid -> Prop :=
  | nonzero_leaf:
    Nonzero (Leaf ONE)
  | nonzero_node_l:
    forall l r,
    Nonzero l ->
    Nonzero (Node l r)
  | nonzero_node_r:
    forall l r,
    Nonzero r ->
    Nonzero (Node l r).

  (**
    An interpretation [Pid i x] holds if [x] is in tree of intervals [i].
    *)

  Inductive Interpret: pid -> Q -> Prop :=
  | interpret_leaf:
    forall x,
    One.One x ->
    Interpret (Leaf ONE) x
  | interpret_node_l:
    forall i1 i2 x,
    Interpret i1 ((2#1) * x) ->
    Interpret (Node i1 i2) x
  | interpret_node_r:
    forall i1 i2 x,
    Interpret i2 ((2#1) * x - 1) ->
    Interpret (Node i1 i2) x.

  Let interpret_left:
    forall i x,
    Interpret (Node (Leaf ZERO) i) x ->
    Interpret i ((2#1)*x-1).
  Proof.
    intros.
    inversion H; subst; clear H; auto.
    inversion H3; subst; clear H3.
  Qed.

  Let interpret_right:
    forall i x,
    Interpret (Node i (Leaf ZERO)) x ->
    Interpret i ((2#1)*x).
  Proof.
    intros.
    inversion H; subst; clear H; auto.
    inversion H3; subst; clear H3.
  Qed.

  Let one_to_leaf:
    forall x,
    One x ->
    Interpret (Leaf ONE) x.
  Proof.
    auto using interpret_leaf.
  Qed.

  Let leaf_to_one:
    forall x,
    Interpret (Leaf ONE) x ->
    One x.
  Proof.
    intros.
    inversion H; subst.
    assumption.
  Qed.

  (** Any interval is intersects with the interval [0, 1). *)

  Lemma interpret_to_one:
    forall i x,
    Interpret i x ->
    One x.
  Proof.
    induction i; intros. {
      inversion H; subst; clear H.
      assumption.
    }
    inversion H; subst; clear H;
      auto using one_split_1, one_split_2.
  Qed.

  (** A tree of intervals is not a canonical representation of a set of
   intervals, therefore the follwoing predicate yields the normalized (i.e.,
   the smallest term) equals the same set of intervals.
   *)

  Inductive Norm: pid -> pid -> Prop :=
  | norm_leaf:
    forall i,
    Norm (Leaf i) (Leaf i)
  | norm_neq:
    forall l r x y,
    Norm l x ->
    Norm r y ->
    x <> y ->
    Norm (Node l r) (Node x y)
  | norm_node_node:
    forall l r l1 r1 l2 r2,
    Norm l (Node l1 r1) ->
    Norm r (Node l2 r2) ->
    Norm (Node l r) (Node (Node l1 r1) (Node l2 r2))
  | norm_eq:
    forall l r b,
    Norm l (Leaf b) ->
    Norm r (Leaf b) ->
    Norm (Node l r) (Leaf b).

  (** We also introduce the predicate that checks if a term is normalized
    already, which happens when the [Norm] is a reflexive. *)

  Inductive Normalized : pid -> Prop :=
  | normalized_def:
    forall i,
    Norm i i ->
    Normalized i.

  (** Split, breaks a pid into two disjoint set of intervals. *)

  Inductive Split: pid -> pid -> pid -> Prop :=
  | split_off:
    Split (Leaf ZERO) (Leaf ZERO) (Leaf ZERO)
  | split_on:
    Split (Leaf ONE) (Node (Leaf ONE) (Leaf ZERO)) (Node (Leaf ZERO) (Leaf ONE))
  | split_left_off:
    forall i i1 i2,
    Split i i1 i2 ->
    Split (Node (Leaf ZERO) i) (Node (Leaf ZERO) i1) (Node (Leaf ZERO) i2)
  | split_right_off:
    forall i i1 i2,
    Split i i1 i2 ->
    Split (Node i (Leaf ZERO)) (Node i1 (Leaf ZERO)) (Node i2 (Leaf ZERO))
  | split_node:
    forall i1 i2,
    i1 <> Leaf ZERO ->
    i2 <> Leaf ZERO ->
    Split (Node i1 i2) (Node i1 (Leaf ZERO)) (Node (Leaf ZERO) i2).

  (** The left-hand side is a sub-set of the original interval. *)

  Lemma split_spec_1:
    forall i i1 i2 (x:Q),
    Split i i1 i2 ->
    Interpret i2 x ->
    Interpret i x.
  Proof.
    induction i; intros. {
      inversion H; subst; clear H;
      auto using one_split_2, leaf_to_one, interpret_leaf.
    }
    inversion H; subst; clear H;
    eauto using interpret_node_l, 
          interpret_node_r.
  Qed.

  (** The right-hand side is a sub-set of the original interval. *)

  Lemma split_spec_2:
    forall i i1 i2 (x:Q),
    Split i i1 i2 ->
    Interpret i1 x ->
    Interpret i x.
  Proof.
    induction i; intros. {
      inversion H; subst; clear H;
      auto using one_split_1, leaf_to_one, interpret_leaf.
    }
    inversion H; subst; clear H;
    eauto using interpret_node_l, 
          interpret_node_r.
  Qed.

  (** The two sets are disjoint, so if we have the right-hand side,
    then the left-hand side cannot hold. *)

  Lemma split_spec_3:
    forall i i1 i2 (x:Q),
    Split i i1 i2 ->
    Interpret i x ->
    Interpret i1 x ->
    ~ Interpret i2 x.
  Proof.
    induction i; intros. {
      inversion H; subst; clear H. {
        inversion H1; subst.
      }
      unfold not; intros.
      apply interpret_right in H1.
      apply interpret_left in H.
      apply leaf_to_one in H.
      apply leaf_to_one in H1.
      apply one_neg_1 in H; auto.
    }
    inversion H; subst; clear H.
    - apply interpret_left in H1.
      eapply IHi2 in H1; eauto.
    - apply interpret_right in H1.
      eapply IHi1 in H1; eauto.
    - apply interpret_right in H1.
      unfold not; intros.
      apply interpret_left in H.
      apply interpret_to_one in H.
      apply interpret_to_one in H1.
      apply one_neg_1 in H; auto.
  Qed.

  (** And the other way around too. *)

  Lemma split_spec_4:
    forall i i1 i2 (x:Q),
    Split i i1 i2 ->
    Interpret i x ->
    Interpret i2 x ->
    ~ Interpret i1 x.
  Proof.
    induction i; intros. {
      inversion H; subst; clear H. {
        inversion H1; subst.
      }
      unfold not; intros.
      apply interpret_left in H1.
      apply interpret_right in H.
      apply leaf_to_one in H.
      apply leaf_to_one in H1.
      apply one_neg_1 in H; auto.
    }
    inversion H; subst; clear H.
    - apply interpret_left in H1.
      eapply IHi2 in H1; eauto.
    - apply interpret_right in H1.
      eapply IHi1 in H1; eauto.
    - apply interpret_left in H1.
      unfold not; intros.
      apply interpret_right in H.
      apply interpret_to_one in H.
      apply interpret_to_one in H1.
      apply one_neg_1 in H; auto.
  Qed.

End Defs.

Section DoSum.

  (** The sum is the reverse operation of a split. Performing
  the interval-wise union of all intervals. *)

  Inductive Sum: pid -> pid -> pid -> Prop :=
  | sum_off_left:
    forall i,
    Sum (Leaf ZERO) i i
  | sum_off_right:
    forall i,
    Sum i (Leaf ZERO) i
  | sum_on:
    forall l r,
    l = Leaf ONE \/ r = Leaf ONE ->
    Sum l r (Leaf ONE)
  | sum_node:
    forall l1 l2 r1 r2 l r,
    Sum l1 l2 l ->
    Sum r1 r2 r ->
    Sum (Node l1 r1) (Node l2 r2) (Node l r).

  (** Thus from the left-hand side argument we can always get to the result. *)

  Lemma sum_spec_1:
    forall i1 i2 i3 (x:Q),
    Sum i1 i2 i3 ->
    Interpret i1 x ->
    Interpret i3 x.
  Proof.
    induction i1; intros. {
      inversion H; subst; clear H.
      - inversion H0.
      - assumption.
      - destruct H1; subst. {
          rewrite H in *.
          assumption.
        }
        inversion H0; subst.
        assumption.
    }
    inversion H; subst; clear H.
    - auto.
    - destruct H1. {
        inversion H.
      }
      subst.
      eauto using interpret_to_one, interpret_leaf.
    - inversion H0; subst; clear H0. {
        eauto using interpret_node_l.
      }
      eauto using interpret_node_r.
  Qed.

  (** And from the right-hand side argument we can always get to the result. *)

  Lemma sum_spec_2:
    forall i2 i1 i3 (x:Q),
    Sum i1 i2 i3 ->
    Interpret i2 x ->
    Interpret i3 x.
  Proof.
    induction i2; intros. {
      inversion H; subst; clear H.
      - assumption.
      - inversion H0.
      - destruct H1; subst. {
          inversion H0; subst.
          assumption.
        }
        rewrite H in *.
        assumption.
    }
    inversion H; subst; clear H.
    - auto.
    - destruct H1. {
        eauto using interpret_to_one, interpret_leaf.
      }
      inversion H.
    - inversion H0; subst; clear H0. {
        eauto using interpret_node_l.
      }
      eauto using interpret_node_r.
  Qed.

  Let sum_inv_off_left:
    forall i j,
    Sum (Leaf ZERO) i j ->
    i = j.
  Proof.
    intros.
    inversion H; subst; clear H; auto.
    destruct i; intuition; auto; inversion H.
  Qed.

  (** The sum predicate behaves like a function. *)

  Lemma sum_fun:
    forall i j k k',
    Sum i j k ->
    Sum i j k' ->
    k = k'.
  Proof.
    induction i; intros. {
      destruct b. {
        inversion H; subst; clear H. {
          inversion H0; subst; clear H0; auto.
        }
        destruct H1; auto; inversion H0; subst; clear H0; auto.
      }
      apply sum_inv_off_left in H.
      apply sum_inv_off_left in H0.
      subst; auto.
    }
    inversion H; subst; clear H;
    inversion H0; subst; clear H0; auto.
    - destruct H; inversion H.
    - destruct H1; inversion H.
    - destruct H1; inversion H.
    - destruct H; inversion H.
    - assert (l = l3) by eauto.
      assert (r = r3) by eauto.
      subst.
      auto.
  Qed.
End DoSum.

(* ---------------------- *)
(* Implementation *)

Section Impl.

  (** In this section we show an implementation of these operators. *)

  Fixpoint interpret (p:pid) (x:Q) : bool :=
  match p with
  | Leaf ONE => one x
  | Leaf ZERO => false
  | Node i1 i2 =>
    (interpret i1 ((2#1) * x)) || (interpret i2 ((2#1)*x - 1))
  end.

  Goal interpret (Leaf ZERO) (32#1) = false % nat.
  Proof.
    auto.
  Qed.

  Goal interpret (Node (Leaf ZERO) (Leaf ONE)) (1#2) = true.
  Proof.
    simpl. auto.
  Qed.

  Goal interpret (Leaf ONE) (1#2) = true.
  Proof.
    simpl. auto.
  Qed.

  Goal interpret (Leaf ONE) (3#2) = false.
  Proof.
    simpl. auto.
  Qed.

  Fixpoint norm (i:pid) : pid :=
  match i with
  | Leaf _ => i
  | Node l r =>
    match (norm l), (norm r) with
    | Leaf ONE, Leaf ONE => Leaf ONE
    | Leaf ZERO, Leaf ZERO => Leaf ZERO
    | _, _ => Node (norm l) (norm r)
    end
  end.
(*
  Lemma interpret_leb:
    forall i x,
    leb (interpret i x) (One.one x).
  Proof.
    induction i; intros.
    + unfold leb.
      destruct b. {
        simpl.
        remember (One.one x).
        destruct b; auto.
      }
      simpl; auto.
    + simpl.
      assert (leb (interpret i1 ((2 # 1) * x)) (One.one ((2 # 1) * x))) by auto.
      assert (leb (interpret i2 ((2 # 1) * x - 1)) (One.one ((2 # 1) * x - 1))) by auto.
      remember (_ * x).
      rewrite <- One.one_split.
      rewrite <- Heqq.
      rewrite leb_implb in *.
      destruct (interpret i1 q); simpl in *. {
        rewrite H in *; auto.
      }
      destruct (interpret i2 (q-1)); simpl in *; auto.
      rewrite orb_comm.
      rewrite H0.
      auto.
  Qed.
*)
  Goal norm (Leaf ONE) = Leaf ONE.
  Proof.
    auto.
  Qed.

  Goal norm (Node (Leaf ONE) (Leaf ONE)) = Leaf ONE.
  Proof.
    auto.
  Qed.

  Goal
    norm (Node (Leaf ONE) (Node (Leaf ONE) (Leaf ONE))) =
    Leaf ONE.
  Proof.
    auto.
  Qed.

  Let normalized_left:
    forall i1 i2,
    Normalized (Node i1 i2) ->
    Normalized i2.
  Proof.
    intros.
    inversion H; subst; clear H.
    inversion H0; subst; clear H0. {
      auto using normalized_def.
    }
    auto using normalized_def.
  Qed.

  Let normalized_right:
    forall i1 i2,
    Normalized (Node i1 i2) ->
    Normalized i1.
  Proof.
    intros.
    inversion H; subst; clear H.
    inversion H0; subst; clear H0. {
      auto using normalized_def.
    }
    auto using normalized_def.
  Qed.

  Let norm_node_off_on:
    forall i1 i2,
    Norm i1 (Leaf ZERO) ->
    Norm i2 (Leaf ONE) ->
    Norm (Node i1 i2) (Node (Leaf ZERO) (Leaf ONE)).
  Proof.
    intros.
    apply norm_neq; auto.
    unfold not; intros N; inversion N.
  Qed.

  Let norm_node_on_off:
    forall i1 i2,
    Norm i1 (Leaf ONE) ->
    Norm i2 (Leaf ZERO) ->
    Norm (Node i1 i2) (Node (Leaf ONE) (Leaf ZERO)).
  Proof.
    intros.
    apply norm_neq; auto.
    unfold not; intros N; inversion N.
  Qed.

  Let norm_leaf_node:
    forall i1 i2 b l r,
    Norm i1 (Leaf b) ->
    Norm i2 (Node l r) ->
    Norm (Node i1 i2) (Node (Leaf b) (Node l r)).
  Proof.
    intros.
    apply norm_neq; auto.
    unfold not; intros N; inversion N.
  Qed.

  Let norm_node_leaf:
    forall i1 i2 b l r,
    Norm i1 (Node l r) ->
    Norm i2 (Leaf b) ->
    Norm (Node i1 i2) (Node (Node l r) (Leaf b)).
  Proof.
    intros.
    apply norm_neq; auto.
    unfold not; intros N; inversion N.
  Qed.

  Lemma norm_to_prop:
    forall i j,
    norm i = j ->
    Norm i j.
  Proof.
    induction i; intros. {
      simpl in *.
      subst.
      auto using norm_leaf.
    }
    simpl in *.
    remember (norm i1).
    assert (Norm i1 p) by auto.
    destruct p. {
      destruct b. {
        remember (norm i2).
        assert (Norm i2 p) by auto.
        destruct p; subst; auto using norm_leaf_node.
        destruct b; subst; auto using norm_eq.
      }
      remember (norm i2).
      destruct p; subst; auto.
      destruct b; auto using norm_eq, norm_node_off_on.
    }
    remember (norm i2).
    assert (Norm i2 p) by auto.
    destruct p; subst; auto using norm_node_node, norm_node_leaf.
  Qed.
(*
  Lemma norm_spec:
    forall i x,
    interpret (norm i) x = interpret i x.
  Proof.
    induction i; intros; simpl; auto.
    rewrite <- IHi1.
    rewrite <- IHi2.
    destruct (norm i1). {
      destruct (norm i2). {
        destruct b;
          destruct b0; simpl; auto using One.one_split.
      }
      destruct b; simpl; auto.
    }
    simpl.
    auto.
  Qed.
*)
  Fixpoint split (i:pid) : pid * pid :=
  match i with
  | Leaf ZERO => (Leaf ZERO, Leaf ZERO)
  | Leaf ONE => (Node (Leaf ONE) (Leaf ZERO), Node (Leaf ZERO) (Leaf ONE))
  | Node (Leaf ZERO) i =>
    let (i1, i2) := split i in
    (Node (Leaf ZERO) i1, Node (Leaf ZERO) i2)
  | Node i (Leaf ZERO) =>
    let (i1, i2) := split i in
    (Node i1 (Leaf ZERO), Node i2 (Leaf ZERO))
  | Node i1 i2 =>
    (Node i1 (Leaf ZERO), Node (Leaf ZERO) i2)
  end.

  Goal (split (Node (Leaf ONE) (Node (Leaf ONE) (Leaf ONE)))) =
    (Node (Leaf ONE) (Leaf ZERO), Node (Leaf ZERO) (Node (Leaf ONE) (Leaf ONE))).
  Proof.
    auto.
  Qed.

  Goal interpret (Node (Leaf ONE) (Leaf ZERO)) (1#4) = true.
  Proof.
    auto.
  Qed.

  Goal interpret (Node (Leaf ZERO) (Node (Leaf ONE) (Leaf ONE))) (1#3) = false.
  Proof.
    auto.
  Qed.

  Let split_inv_right_off: 
    forall (i:pid),
    split (Node i (Leaf ZERO)) = (Node (fst (split i)) (Leaf ZERO), Node (snd (split i)) (Leaf ZERO)).
  Proof.
    induction i; intros. {
      destruct b; simpl; auto.
    }
    simpl.
    destruct i1. {
      destruct b; simpl in *; auto. {
        destruct i2; simpl. {
          destruct b; simpl; auto.
        }
        auto.
      }
      destruct (split i2); auto.
    }
    destruct i2; auto.
    destruct b; simpl in *; auto. {
      destruct i1_1. {
        destruct b; simpl in *; auto. {
          destruct i1_2; auto.
          destruct b; auto.
        }
        destruct (split i1_2); auto.
      }
      destruct i1_2; auto.
      destruct b; auto.
      remember (split _).
      destruct p; auto.
    }
  Qed.

(*
  Let one_absorb:
    forall x i,
    one x || interpret i x = one x.
  Proof.
    intros.
    assert (leb (interpret i x) (one x)) by auto using interpret_leb.
    unfold leb in *.
    destruct (interpret i x). {
      rewrite orb_true_r.
      auto.
    }
    auto using orb_false_r.
  Qed.
*)
  Let leaf_on_off:
    Leaf ONE <> Leaf ZERO.
  Proof.
    unfold not; intros N; inversion N.
  Qed.

  Let leaf_on_node:
    forall i1 i2,
    Node i1 i2 <> Leaf ZERO.
  Proof.
    intros.
    unfold not; intros N; inversion N.
  Qed.

  Lemma split_to_prop:
    forall i,
    Split i (fst (split i)) (snd (split i)).
  Proof.
    induction i; intros. {
      destruct b; simpl; auto using split_on, split_off.
    }
    destruct i1. {
      destruct b; simpl in *. {
        destruct i2. {
          destruct b; simpl in *. {
            auto using split_node, norm_node_on_off.
          }
          auto using split_right_off.
        }
        apply split_node; auto.
      }
      destruct (split i2) as (l, r).
      simpl in *.
      auto using split_left_off.
    }
    destruct i2. {
      destruct b. {
        simpl; auto using split_node.
      }
      rewrite split_inv_right_off in *.
      simpl; auto using split_right_off.
    }
    simpl.
    auto using split_node.
  Qed.


  Fixpoint sum (i1 i2:pid) :=
  match i1 with
  | Leaf ONE => Leaf ONE
  | Leaf ZERO => i2
  | Node l1 r1 =>
    match i2 with
    | Leaf ZERO => i1
    | Leaf ONE => Leaf ONE
    | Node l2 r2 => Node (sum l1 l2) (sum r1 r2)
    end
  end.

  Goal sum (Node (Leaf ONE) (Leaf ZERO)) (Node (Leaf ZERO) (Leaf ONE))
    = Node (Leaf ONE) (Leaf ONE).
  Proof.
    auto.
  Qed.


  Lemma sum_spec:
    forall i j,
    Sum i j (sum i j).
  Proof.
    induction i; intros. {
      destruct b. {
        simpl; auto using sum_on.
      }
      auto using sum_off_left.
    }
    destruct j. {
      destruct b. {
        auto using sum_on.
      }
      auto using sum_off_right.
    }
    simpl.
    auto using sum_node.
  Qed.
End Impl.

(*
  Let interpret_inv_true_ge:
    forall i x,
    Interpret i x ->
    x >= 0.
  Proof.
    induction i; intros. {
      inversion H; subst; clear H.
      inversion H1; subst.
      auto.
    }
    inversion H; subst; clear H.
    - apply IHi1 in H3; lra.
    - apply IHi2 in H3; lra.
  Qed.
*)
(*
  Let interpret_inv_true_lt:
    forall i x,
    Interpret i x ->
    x < 1.
  Proof.
    induction i; intros. {
      inversion H; subst; clear H.
      inversion H1; subst.
      auto.
    }
    inversion H; subst; clear H.
    - apply IHi1 in H3; lra.
    - apply IHi2 in H3; lra.
  Qed.
*)

(** The following represents an axiomatic semantics of an Identity object. *)

Class Ident {A:Type}
  (interpret: A -> Q -> bool)
  (split: A -> A * A)
  (sum: A -> A -> A)
   := {
  split_1:
    forall x y z q,
    split x = (y, z) ->
    xorb (interpret y q) (interpret z q) = true;
  split_2:
    forall x y z q,
    split x = (y, z) ->
    interpret x q = interpret y q || interpret z q;
  sum_1:
    forall x y q,
    interpret (sum x y) q = interpret x q || interpret y q;
}.

(** Show that our implementation follows the specification. *)
(*
Instance TreeIdent : Ident split sum interpret.
Proof.
  apply Build_Ident; eauto using split_spec_3, split_spec_4, sum_spec_2.
Qed.
*)